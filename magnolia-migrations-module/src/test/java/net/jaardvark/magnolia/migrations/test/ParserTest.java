package net.jaardvark.magnolia.migrations.test;

import static org.junit.Assert.*;

import java.io.IOException;

import net.jaardvark.magnolia.migrations.parser.JCRMigrationEngineLexer;
import net.jaardvark.magnolia.migrations.parser.JCRMigrationEngineParser;
import net.jaardvark.magnolia.migrations.parser.JCRMigrationEngineParser.MigrationScriptContext;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

@SuppressWarnings("unused")
public class ParserTest {

	@Test
	public void testParser() throws IOException {
		
		CharStream input = new ANTLRFileStream("sample2.migration");
		JCRMigrationEngineLexer lexer = new JCRMigrationEngineLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		JCRMigrationEngineParser parser = new JCRMigrationEngineParser(tokens);
		
		MigrationScriptContext ctx = parser.migrationScript();
		
	}

}
