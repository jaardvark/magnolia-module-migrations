package net.jaardvark.magnolia.migrations.engine;

import javax.jcr.Node;

import net.jaardvark.magnolia.migrations.rules.RuleList;
import net.jaardvark.magnolia.migrations.rules.RulesRule;

public class MigrationContext {

	protected Node sourceNode;
	protected Node destNode;
	protected RuleList ruleList;
	protected MigrationContext parent;

	public MigrationContext(MigrationContext parent, Node sourceNode, Node destNode, RuleList ruleList) {
		this.parent = parent;
		this.sourceNode = sourceNode;
		this.destNode = destNode;
		this.ruleList = ruleList;
	}
	public MigrationContext getChildContext(RuleList ruleList) {
		return new MigrationContext(this, sourceNode, destNode, ruleList);
	}
	public MigrationContext getChildContext(Node sourceNode) {
		return new MigrationContext(this, sourceNode, destNode, ruleList);
	}
	public MigrationContext getChildContext(Node sourceNode, RuleList ruleList) {
		return new MigrationContext(this, sourceNode, destNode, ruleList);
	}
	public MigrationContext getChildContext(RuleList ruleList, Node destNode) {
		return new MigrationContext(this, sourceNode, destNode, ruleList);
	}
	public MigrationContext getChildContext(Node sourceNode, RuleList ruleList, Node destNode) {
		return new MigrationContext(this, sourceNode, destNode, ruleList);
	}
	public MigrationContext getChildContextWithDestNode(Node destNode) {
		return new MigrationContext(this, sourceNode, destNode, ruleList);
	}	

	public Node getSourceNode() {
		return sourceNode;
	}

	public void setSourceNode(Node sourceNode) {
		this.sourceNode = sourceNode;
	}

	public Node getDestNode() {
		return destNode;
	}

	public void setDestNode(Node destNode) {
		this.destNode = destNode;
	}

	public RuleList getRuleList() {
		return ruleList;
	}

	public void setRuleList(RuleList ruleList) {
		this.ruleList = ruleList;
	}

	public MigrationContext getParent() {
		return parent;
	}

	public void setParent(MigrationContext parent) {
		this.parent = parent;
	}

		
	
	public RulesRule resolveUse(String reference) {
		RulesRule result = null;
		if (ruleList!=null)
			result = ruleList.getUseRule(reference);
		if (result!=null)
			return result;
		MigrationContext p = parent;
		while (p!=null && p.getRuleList()==ruleList)
			p = p.getParent();
		if (p!=null)
			return p.resolveUse(reference);
		return null;
	}

}
