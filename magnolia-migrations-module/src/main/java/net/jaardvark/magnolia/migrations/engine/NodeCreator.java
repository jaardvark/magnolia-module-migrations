package net.jaardvark.magnolia.migrations.engine;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Creates nodes including UUID. This abstraction exists to provide a way
 * for applications to do this in a JCR-implementation independent way, since
 * JCR API doesn't offer a way of doing so, but the implementations generally do.
 * @author Richard Unger
 */
public interface NodeCreator {

	public Node createNode(Node parent, String name, String primaryType, String uuid) throws RepositoryException;
	
}
