package net.jaardvark.magnolia.migrations.module;

import java.util.HashMap;
import java.util.Map;

import net.jaardvark.magnolia.migrations.functions.Function;

public class MigrationsModule {
	
	Map<String, Function> functions = new HashMap<String, Function>();

	public Map<String, Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<String, Function> functions) {
		this.functions = functions;
	}
	
	public void putFunction(String name, Function function){
		functions.put(name, function);
	}
	
}
