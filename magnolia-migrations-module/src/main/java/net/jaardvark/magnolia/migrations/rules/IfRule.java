package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Property;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

import org.apache.jackrabbit.commons.predicate.Predicate;

public class IfRule extends MatchingRuleBase implements ImportRule, RuleListRule, PropertyDecidingRule {

	protected RuleList ruleList;

	public IfRule(Predicate matchConditionPredicate, RuleList ruleList) {
		super(matchConditionPredicate);
		this.ruleList = ruleList;
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {
		// if we matched, apply our rule-list to the matched node
		MigrationContext ifContext = ctx.getChildContext(ruleList);
		ImportResult result = ruleList.ruleListImportStage(ifContext);
		if (result==ImportResult.IMPORTED)
			ctx.setDestNode(ifContext.getDestNode());
		return result;
	}
	
	@Override
	public RuleList getRuleList() {
		return ruleList;
	}

	@Override
	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		MigrationContext ifContext = ctx.getChildContext(ruleList);		
		return ruleList.isPropertyExcluded(ifContext, p);
	}

}
