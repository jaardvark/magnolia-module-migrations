package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Item;
import javax.jcr.Property;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public class UseRule implements ImportRule, PropertyDecidingRule {

	protected String reference;

	public UseRule(String reference){
		this.reference = reference;
	}
	
	public String getReference() {
		return reference;
	}

	@Override
	public boolean matches(Item item) {
		return true;
	}

	@Override
	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		RulesRule rule = ctx.resolveUse(reference);
		MigrationContext useContext = ctx.getChildContext(rule.getRuleList());
		return rule.getRuleList().isPropertyExcluded(useContext, p);
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {
		RulesRule rule = ctx.resolveUse(reference);
		MigrationContext useContext = ctx.getChildContext(rule.getRuleList());
		ImportResult result = rule.getRuleList().ruleListImportStage(useContext);
		if (result==ImportResult.IMPORTED)
			ctx.setDestNode(useContext.getDestNode());
		return result;
	}
	
}
