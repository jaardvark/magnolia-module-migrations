package net.jaardvark.magnolia.migrations.rules;

import info.magnolia.jcr.RuntimeRepositoryException;

import javax.jcr.Item;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import net.jaardvark.jcr.predicate.ValueProvider;
import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public class SetChildPropertyRule implements PropertyDecidingRule, PropertyValueRule {

	protected String propName;
	protected ValueProvider propValue;

	public SetChildPropertyRule(String propName, ValueProvider propValue){
		this.propName = propName;
		this.propValue = propValue;
	}
	
	@Override
	public boolean matches(Item item) {
		try {
			return item instanceof Property && item.getName().equals(propName);
		} catch (RepositoryException e) {
			throw new RuntimeRepositoryException(e);
		}
	}

	@Override
	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		if (matches(p))
			return ImportResult.IMPORTED;
		return ImportResult.NOT_MATCHED;
	}

	@Override
	public Value getPropertyValue(Item p) throws RepositoryException{
		return propValue.getValue(p);
	}

	@Override
	public String getPropertyName() {
		return propName;
	}

	public boolean isMultiple(Item p) throws RepositoryException {
		return propValue.isMultiple(p);
	}

	public Value[] getPropertyValues(Item p) throws RepositoryException {
		return propValue.getValues(p);
	}
}
