package net.jaardvark.magnolia.migrations.rules;

import info.magnolia.jcr.RuntimeRepositoryException;

import java.util.Map;

import javax.jcr.Item;
import javax.jcr.Property;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public class MapRule implements PropertyDecidingRule, PropertyValueRule {

	protected String propName;
	protected Map<ValueKey, Value> mapping;
	protected boolean hadDefault;
	protected Value defaultMapping;

	public MapRule(String propName, Map<ValueKey, Value> mapping, boolean hadDefault, Value defaultMapping){
		this.propName = propName;
		this.mapping = mapping;
		this.hadDefault = hadDefault;
		this.defaultMapping = defaultMapping;
	}
	
	@Override
	public boolean matches(Item item) {
		try {
			return item instanceof Property && item.getName().equals(propName);
		} catch (RepositoryException e) {
			throw new RuntimeRepositoryException(e);
		}
	}

	@Override
	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		if (matches(p))
			return ImportResult.IMPORTED;
		return ImportResult.NOT_MATCHED;
	}

	@Override
	public Value getPropertyValue(Item p) throws RepositoryException {
		Value oVal = ((Property)p).getValue();
		return mapValue(oVal);
	}

	
	public Value mapValue(Value oVal) throws RepositoryException {
		ValueKey oKey = key(oVal);
		if (hadDefault && !mapping.containsKey(oKey))
			return defaultMapping;
		Value val = mapping.get(oKey);
		if (val!=null)
			return val;
		if (mapping.containsKey(oKey))
			return null;
		return oVal;
	}
	
	
	/**
	 * Because the JCR value objects are different for RMI and local, and because they all
	 * have different (IMHO broken) implementations for hashCode() and equals() we build our own key objects
	 * in order to match them.
	 * @param value
	 * @return
	 * @throws RepositoryException 
	 */
	protected static ValueKey key(Value value) {
		return new ValueKey(value);
	}
	
	
	@Override
	public String getPropertyName() {
		return propName;
	}

	public boolean hasMappingFor(Property p) throws RepositoryException {
		return hasMappingFor(p.getValue());
	}
	public boolean hasMappingFor(Value v) throws RepositoryException {
		return hadDefault || mapping.containsKey(key(v));
	}
	
	
	
	
	
	
	
	public static class ValueKey {
		int type = -1;
		String val = null;
		public ValueKey(Value value) {
			if (value!=null){
				type = value.getType();
				if (type==PropertyType.BINARY)
					throw new IllegalArgumentException("Binary values cannot be mapped!");
				try {
					val = value.getString();
				} catch (RepositoryException e) {
					throw new RuntimeRepositoryException(e);
				}
			}
		}
		@Override
		public int hashCode() {
			if (val==null)
				return "null".hashCode() + type;
			return val.hashCode() + type;
		}
		@Override
		public boolean equals(Object o) {
			if (o instanceof ValueKey){
				if (((ValueKey) o).val==null)
					return val==null;
				return ((ValueKey) o).val.equals(val) && ((ValueKey) o).type==type;
			}
			return false;
		}
		
	}

	
	
	
}










