package net.jaardvark.magnolia.migrations.module;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.SetPropertyTask;
import info.magnolia.module.delta.Task;

import java.util.ArrayList;
import java.util.List;

public class MigrationsModuleVersionHandler  extends DefaultModuleVersionHandler {
	
	@Override
	protected List<Task> getExtraInstallTasks(InstallContext installContext) {
		List<Task> extraTasks = new ArrayList<Task>(); 
		extraTasks.add(new SetPropertyTask("Set mapping for plain text resources.", "config", "/modules/resources/apps/resources/subApps/browser/actions/editResource/subAppMapping", "text" , "text"));
		return extraTasks;
	}

}
