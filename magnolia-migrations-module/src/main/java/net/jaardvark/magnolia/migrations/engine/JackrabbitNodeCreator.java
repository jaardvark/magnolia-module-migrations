package net.jaardvark.magnolia.migrations.engine;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.jackrabbit.core.NodeImpl;

public class JackrabbitNodeCreator implements NodeCreator {

	@Override
	public Node createNode(Node parent, String name, String primaryType, String uuid) throws RepositoryException {
		NodeImpl parentImpl = (NodeImpl) parent;
		return parentImpl.addNodeWithUuid(name, primaryType, uuid);
	}

}
