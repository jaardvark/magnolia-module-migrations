package net.jaardvark.magnolia.migrations.functions;

import info.magnolia.jcr.RuntimeRepositoryException;

import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.jackrabbit.value.ValueFactoryImpl;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * Strips all HTML from the input value (assumed to be a string).
 * Uses JSoup library to do so.
 * Always returns a String value.
 * @author Richard Unger
 */
public class StripHTMLFunction implements Function {

	@Override
	public Integer getExpectedNumberOfArguments(){
		return 1;
	}
	
	@Override
	public Value apply(Value...input) {
		try {
			String htmlString = input[0].getString();
			String plainString = Jsoup.clean(htmlString, Whitelist.none());			
			plainString = StringEscapeUtils.unescapeHtml4(plainString);
			Value plainValue = ValueFactoryImpl.getInstance().createValue(plainString);
			return plainValue;
		}
		catch (RepositoryException e) {
			throw new RuntimeRepositoryException(e);
		}
	}

	@Override
	public Value[] applyMultiple(Value... params) {
		throw new UnsupportedOperationException("StripHTMLFunction doesn't support multiple values.");
	}

	@Override
	public boolean isMultiple() {
		return false;
	}

}
