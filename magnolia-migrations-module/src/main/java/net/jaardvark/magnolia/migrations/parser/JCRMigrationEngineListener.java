// Generated from JCRMigrationEngine.g4 by ANTLR 4.5.1

	package net.jaardvark.magnolia.migrations.parser;
	
	import net.jaardvark.magnolia.migrations.rules.*;
	import net.jaardvark.magnolia.migrations.rules.MapRule.ValueKey;
	import net.jaardvark.magnolia.migrations.engine.PredicateHelper;
	import net.jaardvark.magnolia.migrations.engine.RuleHelper;
	import net.jaardvark.magnolia.migrations.engine.ValueHelper;
	import net.jaardvark.magnolia.migrations.engine.MigrationScript;
	import org.apache.jackrabbit.commons.predicate.Predicate;
	import javax.jcr.Value;
	import java.util.Map;
	import java.util.LinkedHashMap;
	import net.jaardvark.jcr.predicate.NodeTypePredicate;
	import net.jaardvark.jcr.predicate.DepthPredicate;
	import net.jaardvark.jcr.predicate.NodePositionPredicate;
	import net.jaardvark.jcr.predicate.WorkspaceNamePredicate;
    import net.jaardvark.jcr.predicate.LiteralValueProvider;
    import net.jaardvark.jcr.predicate.CastingValueProvider;
	import net.jaardvark.jcr.predicate.PropertyValueProvider;
	import net.jaardvark.jcr.predicate.ValueProvider;
	import net.jaardvark.jcr.predicate.Operator;
	
	import static net.jaardvark.magnolia.migrations.engine.ValueHelper.str;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link JCRMigrationEngineParser}.
 */
public interface JCRMigrationEngineListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#migrationScript}.
	 * @param ctx the parse tree
	 */
	void enterMigrationScript(JCRMigrationEngineParser.MigrationScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#migrationScript}.
	 * @param ctx the parse tree
	 */
	void exitMigrationScript(JCRMigrationEngineParser.MigrationScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#migrateStatement}.
	 * @param ctx the parse tree
	 */
	void enterMigrateStatement(JCRMigrationEngineParser.MigrateStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#migrateStatement}.
	 * @param ctx the parse tree
	 */
	void exitMigrateStatement(JCRMigrationEngineParser.MigrateStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#migrationRuleset}.
	 * @param ctx the parse tree
	 */
	void enterMigrationRuleset(JCRMigrationEngineParser.MigrationRulesetContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#migrationRuleset}.
	 * @param ctx the parse tree
	 */
	void exitMigrationRuleset(JCRMigrationEngineParser.MigrationRulesetContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#migrationStatement}.
	 * @param ctx the parse tree
	 */
	void enterMigrationStatement(JCRMigrationEngineParser.MigrationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#migrationStatement}.
	 * @param ctx the parse tree
	 */
	void exitMigrationStatement(JCRMigrationEngineParser.MigrationStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#importingRule}.
	 * @param ctx the parse tree
	 */
	void enterImportingRule(JCRMigrationEngineParser.ImportingRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#importingRule}.
	 * @param ctx the parse tree
	 */
	void exitImportingRule(JCRMigrationEngineParser.ImportingRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#createStatement}.
	 * @param ctx the parse tree
	 */
	void enterCreateStatement(JCRMigrationEngineParser.CreateStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#createStatement}.
	 * @param ctx the parse tree
	 */
	void exitCreateStatement(JCRMigrationEngineParser.CreateStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#createRuleset}.
	 * @param ctx the parse tree
	 */
	void enterCreateRuleset(JCRMigrationEngineParser.CreateRulesetContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#createRuleset}.
	 * @param ctx the parse tree
	 */
	void exitCreateRuleset(JCRMigrationEngineParser.CreateRulesetContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#wrapStatements}.
	 * @param ctx the parse tree
	 */
	void enterWrapStatements(JCRMigrationEngineParser.WrapStatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#wrapStatements}.
	 * @param ctx the parse tree
	 */
	void exitWrapStatements(JCRMigrationEngineParser.WrapStatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#wrapStatement}.
	 * @param ctx the parse tree
	 */
	void enterWrapStatement(JCRMigrationEngineParser.WrapStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#wrapStatement}.
	 * @param ctx the parse tree
	 */
	void exitWrapStatement(JCRMigrationEngineParser.WrapStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#orderSpec}.
	 * @param ctx the parse tree
	 */
	void enterOrderSpec(JCRMigrationEngineParser.OrderSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#orderSpec}.
	 * @param ctx the parse tree
	 */
	void exitOrderSpec(JCRMigrationEngineParser.OrderSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#mapRuleset}.
	 * @param ctx the parse tree
	 */
	void enterMapRuleset(JCRMigrationEngineParser.MapRulesetContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#mapRuleset}.
	 * @param ctx the parse tree
	 */
	void exitMapRuleset(JCRMigrationEngineParser.MapRulesetContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#mapStatement}.
	 * @param ctx the parse tree
	 */
	void enterMapStatement(JCRMigrationEngineParser.MapStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#mapStatement}.
	 * @param ctx the parse tree
	 */
	void exitMapStatement(JCRMigrationEngineParser.MapStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#conditions}.
	 * @param ctx the parse tree
	 */
	void enterConditions(JCRMigrationEngineParser.ConditionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#conditions}.
	 * @param ctx the parse tree
	 */
	void exitConditions(JCRMigrationEngineParser.ConditionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#conditionsTail}.
	 * @param ctx the parse tree
	 */
	void enterConditionsTail(JCRMigrationEngineParser.ConditionsTailContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#conditionsTail}.
	 * @param ctx the parse tree
	 */
	void exitConditionsTail(JCRMigrationEngineParser.ConditionsTailContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(JCRMigrationEngineParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(JCRMigrationEngineParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#valueComparison}.
	 * @param ctx the parse tree
	 */
	void enterValueComparison(JCRMigrationEngineParser.ValueComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#valueComparison}.
	 * @param ctx the parse tree
	 */
	void exitValueComparison(JCRMigrationEngineParser.ValueComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(JCRMigrationEngineParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(JCRMigrationEngineParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#castSpec}.
	 * @param ctx the parse tree
	 */
	void enterCastSpec(JCRMigrationEngineParser.CastSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#castSpec}.
	 * @param ctx the parse tree
	 */
	void exitCastSpec(JCRMigrationEngineParser.CastSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#valueSpec}.
	 * @param ctx the parse tree
	 */
	void enterValueSpec(JCRMigrationEngineParser.ValueSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#valueSpec}.
	 * @param ctx the parse tree
	 */
	void exitValueSpec(JCRMigrationEngineParser.ValueSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#valueList}.
	 * @param ctx the parse tree
	 */
	void enterValueList(JCRMigrationEngineParser.ValueListContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#valueList}.
	 * @param ctx the parse tree
	 */
	void exitValueList(JCRMigrationEngineParser.ValueListContext ctx);
	/**
	 * Enter a parse tree produced by {@link JCRMigrationEngineParser#literalValue}.
	 * @param ctx the parse tree
	 */
	void enterLiteralValue(JCRMigrationEngineParser.LiteralValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link JCRMigrationEngineParser#literalValue}.
	 * @param ctx the parse tree
	 */
	void exitLiteralValue(JCRMigrationEngineParser.LiteralValueContext ctx);
}