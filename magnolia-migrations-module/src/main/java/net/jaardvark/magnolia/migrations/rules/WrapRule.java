package net.jaardvark.magnolia.migrations.rules;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

import org.apache.jackrabbit.commons.predicate.Predicate;

public class WrapRule extends IncludeRule {

	protected List<WrappingNode> wrappingElements;

	public WrapRule(Predicate matchConditionPredicate, RuleList ruleList, List<WrappingNode> wrappingElements) {
		super(matchConditionPredicate, ruleList);
		this.wrappingElements = wrappingElements;
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {
		// create our wrapping-nodes first, if they don't already exist
		Node destNode = createWrappingNodesIfNecessary(ctx.getDestNode());
		MigrationContext childContext = ctx.getChildContextWithDestNode(destNode);
		ImportResult result = super.importStage(childContext);
		ctx.setDestNode(childContext.getDestNode()); // remember the destination node
		return result;
	}
	
	protected Node createWrappingNodesIfNecessary(Node destNode) throws RepositoryException {
		for (WrappingNode wn : wrappingElements){
			if (destNode.hasNode(wn.getName())){
				log.debug("Wrapper node "+wn.getName()+" type "+wn.getPrimaryType()+" already exists.");
				destNode = destNode.getNode(wn.getName());
			}
			else {
				log.debug("Creating wrapper node "+wn.getName()+" type "+wn.getPrimaryType());
				destNode = nodeCreator.createNode(destNode, wn.getName(), wn.getPrimaryType(), null);
			}
		}
		return destNode;
	}

	public static class WrappingNode {
		protected String name;
		protected String primaryType;
		public WrappingNode(String name, String primaryType){
			this.name = name;
			this.primaryType = primaryType;			
		}
		public String getName() { return name; }
		public String getPrimaryType() { return primaryType; }
	}




}
