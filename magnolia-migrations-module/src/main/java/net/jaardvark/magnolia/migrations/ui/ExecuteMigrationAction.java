package net.jaardvark.magnolia.migrations.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Item;

import info.magnolia.objectfactory.Components;
import info.magnolia.rendering.engine.RenderException;
import info.magnolia.resourceloader.Resource;
import info.magnolia.resourceloader.ResourceOrigin;
import info.magnolia.resources.app.action.AbstractResourceAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import net.jaardvark.magnolia.migrations.engine.MigrationHelper;
import net.jaardvark.magnolia.migrations.engine.MigrationScript;
import net.jaardvark.magnolia.migrations.engine.MigrationScriptSyntaxException;

public class ExecuteMigrationAction<D extends ExecuteMigrationActionDefinition> extends AbstractResourceAction<D>{
	
	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(ExecuteMigrationAction.class);
	
	protected Item resourceItem;

	@Inject
	protected ExecuteMigrationAction(D definition, Item resourceItem, @SuppressWarnings("rawtypes") ResourceOrigin origin) {
		super(definition, origin);
		this.resourceItem = resourceItem;
	}

	@Override
	public void execute() throws ActionExecutionException {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Resource resource = unwrapResource(resourceItem);
			log.info("Executing migration script: "+resource.getPath()+" Time: "+sdf.format(new Date()));
			
			//String script = nodeItemToEdit.getJcrItem().getProperty("text").getString();
			MigrationHelper helper = Components.newInstance(MigrationHelper.class);
			MigrationScript script = helper.loadMigrationScript(resource);
			helper.runMigrationScript(script);
			
			log.info("Finished executing migration script: "+resource.getPath()+" Time: "+sdf.format(new Date()));
		} catch (RenderException e) {
			//log.error("Problem loading migration script.",e);
			throw new ActionExecutionException("Problem loading migration script: "+e.getMessage(), e);
		} catch (MigrationScriptSyntaxException e) {
			//log.error("Problem loading migration script.",e);
			throw new ActionExecutionException(e.getMessage(), e);
		} catch (Exception e) {
			//log.error("Problem executing migration script.",e);
			throw new ActionExecutionException("Problem executing migration script: "+e.getMessage(), e);
		}
	}

}
