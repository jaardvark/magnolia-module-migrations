package net.jaardvark.magnolia.migrations.engine;

import java.util.BitSet;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

public class MigrationScriptCompileErrorListener implements ANTLRErrorListener {

	protected StringBuilder sb = new StringBuilder();
	
	protected boolean error = false;
	
	@Override
	public void reportAmbiguity(Parser arg0, DFA arg1, int arg2, int arg3, boolean arg4, BitSet arg5, ATNConfigSet arg6) {
		// ?
	}

	@Override
	public void reportAttemptingFullContext(Parser arg0, DFA arg1, int arg2, int arg3, BitSet arg4, ATNConfigSet arg5) {
		// ?
	}

	@Override
	public void reportContextSensitivity(Parser arg0, DFA arg1, int arg2, int arg3, int arg4, ATNConfigSet arg5) {
		// ?
	}

	@Override
	public void syntaxError(Recognizer<?, ?> arg0, Object arg1, int line, int pos, String msg, RecognitionException ex) {
		sb.append(line+":"+pos+" "+msg+" ("+ex+")");
		sb.append("\n");
		error = true;
	}

	public boolean isError() {
		return error;
	}

	public String getErrorMessages() {
		return sb.toString();
	}
	
}
