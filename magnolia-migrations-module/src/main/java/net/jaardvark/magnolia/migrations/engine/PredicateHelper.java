package net.jaardvark.magnolia.migrations.engine;

import info.magnolia.jcr.predicate.NodeNamePredicate;
import net.jaardvark.jcr.predicate.AndPredicate;
import net.jaardvark.jcr.predicate.DescendantNodeExistsPredicate;
import net.jaardvark.jcr.predicate.NamedPropertyPredicate;
import net.jaardvark.jcr.predicate.NodeHasPropertyPredicate;
import net.jaardvark.jcr.predicate.NodeTypePredicate;
import net.jaardvark.jcr.predicate.NotPredicate;
import net.jaardvark.jcr.predicate.Operator;
import net.jaardvark.jcr.predicate.OrPredicate;
import net.jaardvark.jcr.predicate.ValueProvider;
import net.jaardvark.jcr.predicate.XorPredicate;

import org.apache.jackrabbit.commons.predicate.DepthPredicate;
import org.apache.jackrabbit.commons.predicate.Predicate;

/**
 * Helps building conditions out of predicates. Checks that the types (node/property) don't
 * get mixed in ways that wouldn't work.
 */
public class PredicateHelper {

	public static Predicate conjunction(Predicate leftSide, Predicate predicate) {
		return new AndPredicate(leftSide, predicate);
	}

	public static Predicate disjunction(Predicate leftSide, Predicate predicate) {
		return new OrPredicate(leftSide, predicate);
	}

	public static Predicate negate(Predicate predicate) {
		return new NotPredicate(predicate);
	}

	public static Predicate xor(Predicate leftSide, Predicate predicate) {
		return new XorPredicate(leftSide, predicate);
	}

	public static Predicate nodeNamePredicate(String name, String type) {
		if (type==null)
			return new NodeNamePredicate(name);
		return new AndPredicate(new NodeNamePredicate(name), new NodeTypePredicate(type));
	}

	public static Predicate propertyPredicate(String name, Operator operator, ValueProvider valueProvider) {
		if (valueProvider==null)
			return new NamedPropertyPredicate(name);
		return new NamedPropertyPredicate(valueProvider, operator, name);
	}

	public static Predicate withPropertyPredicate(String name, Operator operator, ValueProvider valueProvider) {
		if (valueProvider==null)
			return new NodeHasPropertyPredicate(name);
		return new NodeHasPropertyPredicate(name, valueProvider, operator);
	}

	
	public static Predicate withNodePredicate(String name, String type) {
		return new DescendantNodeExistsPredicate(name, type);
	}

	public static Predicate depthPredicate(Long min, Long max) {
		if (max!=null)
			return new DepthPredicate(min.intValue(), max.intValue());
		return new DepthPredicate(min.intValue(), min.intValue());		
	}
	
}



