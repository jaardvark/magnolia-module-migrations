package net.jaardvark.magnolia.migrations.ui;

import info.magnolia.ui.contentapp.detail.action.AbstractItemActionDefinition;

public class ExecuteMigrationActionDefinition extends AbstractItemActionDefinition {

	public ExecuteMigrationActionDefinition(){
		setImplementationClass(ExecuteMigrationAction.class);
	}
	
}
