package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Item;

/**
 * A rule supporting conditions which can be matched against a jcr item.
 * @author runger
 */
public interface MatchingRule extends Rule {

	public boolean matches(Item item);

}
