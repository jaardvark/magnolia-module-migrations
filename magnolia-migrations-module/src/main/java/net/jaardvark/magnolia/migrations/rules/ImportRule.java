package net.jaardvark.magnolia.migrations.rules;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public interface ImportRule extends MatchingRule {

	public ImportResult importStage(MigrationContext ctx) throws Exception;
	
}
