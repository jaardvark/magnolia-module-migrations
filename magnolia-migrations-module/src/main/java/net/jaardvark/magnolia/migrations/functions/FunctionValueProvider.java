package net.jaardvark.magnolia.migrations.functions;

import javax.jcr.Item;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import net.jaardvark.jcr.predicate.ValueProvider;

public class FunctionValueProvider implements ValueProvider {

	protected Function function;
	
	protected ValueProvider[] valueProviders;
	
	
	public FunctionValueProvider(Function function, ValueProvider...valueProviders) {
		this.valueProviders = valueProviders;
		this.function = function;
	}
	
	
	@Override
	public Value getValue(Item evaluatedItem) throws RepositoryException {
		Value[] inputValues = new Value[valueProviders.length];
		int i = 0;
		for (ValueProvider valueProvider : valueProviders)
			inputValues[i++] = valueProvider.getValue(evaluatedItem);
		Value outputValue = function.apply(inputValues);
		return outputValue;
	}


	@Override
	public boolean isMultiple(Item item) {
		return function.isMultiple();
	}


	@Override
	public Value[] getValues(Item evaluatedItem) throws RepositoryException {
		Value[] inputValues = new Value[valueProviders.length];
		int i = 0;
		for (ValueProvider valueProvider : valueProviders)
			inputValues[i++] = valueProvider.getValue(evaluatedItem);
		Value[] outputValue = function.applyMultiple(inputValues);
		return outputValue;
	}

}
