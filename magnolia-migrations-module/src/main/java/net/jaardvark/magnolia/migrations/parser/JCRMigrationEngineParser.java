// Generated from JCRMigrationEngine.g4 by ANTLR 4.5.1

	package net.jaardvark.magnolia.migrations.parser;
	
	import net.jaardvark.magnolia.migrations.rules.*;
	import net.jaardvark.magnolia.migrations.rules.MapRule.ValueKey;
	import net.jaardvark.magnolia.migrations.engine.PredicateHelper;
	import net.jaardvark.magnolia.migrations.engine.RuleHelper;
	import net.jaardvark.magnolia.migrations.engine.ValueHelper;
	import net.jaardvark.magnolia.migrations.engine.MigrationScript;
	import org.apache.jackrabbit.commons.predicate.Predicate;
	import javax.jcr.Value;
	import java.util.Map;
	import java.util.LinkedHashMap;
	import net.jaardvark.jcr.predicate.NodeTypePredicate;
	import net.jaardvark.jcr.predicate.DepthPredicate;
	import net.jaardvark.jcr.predicate.NodePositionPredicate;
	import net.jaardvark.jcr.predicate.WorkspaceNamePredicate;
    import net.jaardvark.jcr.predicate.LiteralValueProvider;
    import net.jaardvark.jcr.predicate.CastingValueProvider;
	import net.jaardvark.jcr.predicate.PropertyValueProvider;
	import net.jaardvark.jcr.predicate.ValueProvider;
	import net.jaardvark.jcr.predicate.Operator;
	
	import static net.jaardvark.magnolia.migrations.engine.ValueHelper.str;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JCRMigrationEngineParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, WS=57, BOOL=58, LONG=59, DOUBLE=60, 
		STRING=61, FUNCNAME=62, BlockComment=63, LineComment=64;
	public static final int
		RULE_migrationScript = 0, RULE_migrateStatement = 1, RULE_migrationRuleset = 2, 
		RULE_migrationStatement = 3, RULE_importingRule = 4, RULE_createStatement = 5, 
		RULE_createRuleset = 6, RULE_wrapStatements = 7, RULE_wrapStatement = 8, 
		RULE_orderSpec = 9, RULE_mapRuleset = 10, RULE_mapStatement = 11, RULE_conditions = 12, 
		RULE_conditionsTail = 13, RULE_condition = 14, RULE_valueComparison = 15, 
		RULE_value = 16, RULE_castSpec = 17, RULE_valueSpec = 18, RULE_valueList = 19, 
		RULE_literalValue = 20;
	public static final String[] ruleNames = {
		"migrationScript", "migrateStatement", "migrationRuleset", "migrationStatement", 
		"importingRule", "createStatement", "createRuleset", "wrapStatements", 
		"wrapStatement", "orderSpec", "mapRuleset", "mapStatement", "conditions", 
		"conditionsTail", "condition", "valueComparison", "value", "castSpec", 
		"valueSpec", "valueList", "literalValue"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'migration'", "';'", "'migrate'", "'->'", "'{'", "'}'", "'exclude'", 
		"'delete'", "'rules'", "'rename'", "'node'", "'property'", "'use'", "'non-consuming'", 
		"'if'", "'map'", "'set'", "'='", "'create'", "'type'", "'warn'", "'message'", 
		"'include'", "'wrap'", "'in'", "'unwrap'", "','", "'order'", "'after'", 
		"'before'", "'default'", "'not'", "'('", "')'", "'and'", "'or'", "'xor'", 
		"'with'", "'position'", "'depth'", "'to'", "'workspace'", "'=='", "'>'", 
		"'<'", "'>='", "'<='", "'!='", "'~='", "'Date'", "'Boolean'", "'Long'", 
		"'String'", "'Double'", "'Binary'", "'null'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "WS", "BOOL", "LONG", 
		"DOUBLE", "STRING", "FUNCNAME", "BlockComment", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "JCRMigrationEngine.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public JCRMigrationEngineParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class MigrationScriptContext extends ParserRuleContext {
		public MigrationScript script;
		public Token STRING;
		public MigrationStatementContext migrationStatement;
		public TerminalNode STRING() { return getToken(JCRMigrationEngineParser.STRING, 0); }
		public TerminalNode EOF() { return getToken(JCRMigrationEngineParser.EOF, 0); }
		public List<MigrateStatementContext> migrateStatement() {
			return getRuleContexts(MigrateStatementContext.class);
		}
		public MigrateStatementContext migrateStatement(int i) {
			return getRuleContext(MigrateStatementContext.class,i);
		}
		public List<MigrationStatementContext> migrationStatement() {
			return getRuleContexts(MigrationStatementContext.class);
		}
		public MigrationStatementContext migrationStatement(int i) {
			return getRuleContext(MigrationStatementContext.class,i);
		}
		public MigrationScriptContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_migrationScript; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterMigrationScript(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitMigrationScript(this);
		}
	}

	public final MigrationScriptContext migrationScript() throws RecognitionException {
		MigrationScriptContext _localctx = new MigrationScriptContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_migrationScript);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42);
			match(T__0);
			setState(43);
			((MigrationScriptContext)_localctx).STRING = match(STRING);
			 ((MigrationScriptContext)_localctx).script =  new MigrationScript(str((((MigrationScriptContext)_localctx).STRING!=null?((MigrationScriptContext)_localctx).STRING.getText():null))); 
			setState(45);
			match(T__1);
			setState(49);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(46);
				migrateStatement(_localctx.script);
				}
				}
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(57);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__18) | (1L << T__20) | (1L << T__22) | (1L << T__23) | (1L << T__25))) != 0)) {
				{
				{
				setState(52);
				((MigrationScriptContext)_localctx).migrationStatement = migrationStatement();
				 _localctx.script.addRule(((MigrationScriptContext)_localctx).migrationStatement.rule); 
				}
				}
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(60);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MigrateStatementContext extends ParserRuleContext {
		public MigrationScript script;
		public Token f;
		public Token t;
		public List<TerminalNode> STRING() { return getTokens(JCRMigrationEngineParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(JCRMigrationEngineParser.STRING, i);
		}
		public MigrateStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MigrateStatementContext(ParserRuleContext parent, int invokingState, MigrationScript script) {
			super(parent, invokingState);
			this.script = script;
		}
		@Override public int getRuleIndex() { return RULE_migrateStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterMigrateStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitMigrateStatement(this);
		}
	}

	public final MigrateStatementContext migrateStatement(MigrationScript script) throws RecognitionException {
		MigrateStatementContext _localctx = new MigrateStatementContext(_ctx, getState(), script);
		enterRule(_localctx, 2, RULE_migrateStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			match(T__2);
			setState(63);
			((MigrateStatementContext)_localctx).f = match(STRING);
			setState(64);
			match(T__3);
			setState(65);
			((MigrateStatementContext)_localctx).t = match(STRING);
			setState(66);
			match(T__1);
			 _localctx.script.addMigrate(str((((MigrateStatementContext)_localctx).f!=null?((MigrateStatementContext)_localctx).f.getText():null)), str((((MigrateStatementContext)_localctx).t!=null?((MigrateStatementContext)_localctx).t.getText():null))); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MigrationRulesetContext extends ParserRuleContext {
		public RuleList ruleList;
		public MigrationStatementContext it;
		public List<MigrationStatementContext> migrationStatement() {
			return getRuleContexts(MigrationStatementContext.class);
		}
		public MigrationStatementContext migrationStatement(int i) {
			return getRuleContext(MigrationStatementContext.class,i);
		}
		public MigrationRulesetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_migrationRuleset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterMigrationRuleset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitMigrationRuleset(this);
		}
	}

	public final MigrationRulesetContext migrationRuleset() throws RecognitionException {
		MigrationRulesetContext _localctx = new MigrationRulesetContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_migrationRuleset);
		 ((MigrationRulesetContext)_localctx).ruleList =  new RuleList(); 
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			match(T__4);
			setState(75);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__18) | (1L << T__20) | (1L << T__22) | (1L << T__23) | (1L << T__25))) != 0)) {
				{
				{
				setState(70);
				((MigrationRulesetContext)_localctx).it = migrationStatement();
				 _localctx.ruleList.addRule(((MigrationRulesetContext)_localctx).it.rule); 
				}
				}
				setState(77);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(78);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MigrationStatementContext extends ParserRuleContext {
		public Rule rule;
		public ConditionsContext conditions;
		public Token STRING;
		public MigrationRulesetContext migrationRuleset;
		public Token f;
		public ValueContext v;
		public Token t;
		public ImportingRuleContext importingRule;
		public MapRulesetContext mapRuleset;
		public ValueContext value;
		public Token n;
		public CreateRulesetContext createRuleset;
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public List<TerminalNode> STRING() { return getTokens(JCRMigrationEngineParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(JCRMigrationEngineParser.STRING, i);
		}
		public MigrationRulesetContext migrationRuleset() {
			return getRuleContext(MigrationRulesetContext.class,0);
		}
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ImportingRuleContext importingRule() {
			return getRuleContext(ImportingRuleContext.class,0);
		}
		public MapRulesetContext mapRuleset() {
			return getRuleContext(MapRulesetContext.class,0);
		}
		public CreateRulesetContext createRuleset() {
			return getRuleContext(CreateRulesetContext.class,0);
		}
		public MigrationStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_migrationStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterMigrationStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitMigrationStatement(this);
		}
	}

	public final MigrationStatementContext migrationStatement() throws RecognitionException {
		MigrationStatementContext _localctx = new MigrationStatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_migrationStatement);
		int _la;
		try {
			setState(164);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				match(T__6);
				setState(81);
				((MigrationStatementContext)_localctx).conditions = conditions();
				setState(82);
				match(T__1);
				 ((MigrationStatementContext)_localctx).rule =  RuleHelper.excludeRule(((MigrationStatementContext)_localctx).conditions.predicate); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(85);
				match(T__7);
				setState(86);
				conditions();
				setState(87);
				match(T__1);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(89);
				match(T__8);
				setState(90);
				((MigrationStatementContext)_localctx).STRING = match(STRING);
				setState(91);
				((MigrationStatementContext)_localctx).migrationRuleset = migrationRuleset();
				 ((MigrationStatementContext)_localctx).rule =  new RulesRule(str((((MigrationStatementContext)_localctx).STRING!=null?((MigrationStatementContext)_localctx).STRING.getText():null)), ((MigrationStatementContext)_localctx).migrationRuleset.ruleList); 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(94);
				match(T__9);
				setState(95);
				match(T__10);
				setState(96);
				((MigrationStatementContext)_localctx).f = match(STRING);
				setState(97);
				match(T__3);
				setState(98);
				((MigrationStatementContext)_localctx).v = value();
				setState(99);
				match(T__1);
				 ((MigrationStatementContext)_localctx).rule =  RuleHelper.renameNodeRule(str((((MigrationStatementContext)_localctx).f!=null?((MigrationStatementContext)_localctx).f.getText():null)),((MigrationStatementContext)_localctx).v.valueProvider); 
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(102);
				match(T__9);
				setState(103);
				match(T__11);
				setState(104);
				((MigrationStatementContext)_localctx).f = match(STRING);
				setState(105);
				match(T__3);
				setState(106);
				((MigrationStatementContext)_localctx).t = match(STRING);
				setState(107);
				match(T__1);
				 ((MigrationStatementContext)_localctx).rule =  RuleHelper.renamePropertyRule(str((((MigrationStatementContext)_localctx).f!=null?((MigrationStatementContext)_localctx).f.getText():null)),str((((MigrationStatementContext)_localctx).t!=null?((MigrationStatementContext)_localctx).t.getText():null))); 
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(109);
				match(T__12);
				setState(110);
				((MigrationStatementContext)_localctx).STRING = match(STRING);
				setState(111);
				match(T__1);
				 ((MigrationStatementContext)_localctx).rule =  new UseRule(str((((MigrationStatementContext)_localctx).STRING!=null?((MigrationStatementContext)_localctx).STRING.getText():null))); 
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(113);
				((MigrationStatementContext)_localctx).importingRule = importingRule();
				 ((MigrationStatementContext)_localctx).rule =  ((MigrationStatementContext)_localctx).importingRule.rule; 
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(116);
				match(T__13);
				setState(117);
				((MigrationStatementContext)_localctx).importingRule = importingRule();
				 ((MigrationStatementContext)_localctx).rule =  new NonConsumingRule(((MigrationStatementContext)_localctx).importingRule.rule); 
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(120);
				match(T__14);
				setState(121);
				((MigrationStatementContext)_localctx).conditions = conditions();
				setState(122);
				((MigrationStatementContext)_localctx).migrationRuleset = migrationRuleset();
				 ((MigrationStatementContext)_localctx).rule =  new IfRule(((MigrationStatementContext)_localctx).conditions.predicate, ((MigrationStatementContext)_localctx).migrationRuleset.ruleList); 
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(125);
				match(T__15);
				setState(127);
				_la = _input.LA(1);
				if (_la==T__11) {
					{
					setState(126);
					match(T__11);
					}
				}

				setState(129);
				((MigrationStatementContext)_localctx).STRING = match(STRING);
				setState(130);
				((MigrationStatementContext)_localctx).mapRuleset = mapRuleset();
				 ((MigrationStatementContext)_localctx).rule =  new MapRule(str((((MigrationStatementContext)_localctx).STRING!=null?((MigrationStatementContext)_localctx).STRING.getText():null)), ((MigrationStatementContext)_localctx).mapRuleset.mapping, ((MigrationStatementContext)_localctx).mapRuleset.hadDefault, ((MigrationStatementContext)_localctx).mapRuleset.defaultMapping); 
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(133);
				match(T__16);
				setState(135);
				_la = _input.LA(1);
				if (_la==T__11) {
					{
					setState(134);
					match(T__11);
					}
				}

				setState(137);
				((MigrationStatementContext)_localctx).STRING = match(STRING);
				setState(138);
				match(T__17);
				setState(139);
				((MigrationStatementContext)_localctx).value = value();
				setState(140);
				match(T__1);
				 ((MigrationStatementContext)_localctx).rule =  RuleHelper.setPropertyRule(str((((MigrationStatementContext)_localctx).STRING!=null?((MigrationStatementContext)_localctx).STRING.getText():null)), ((MigrationStatementContext)_localctx).value.valueProvider); 
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(143);
				match(T__18);
				setState(145);
				_la = _input.LA(1);
				if (_la==T__10) {
					{
					setState(144);
					match(T__10);
					}
				}

				setState(147);
				((MigrationStatementContext)_localctx).n = match(STRING);
				setState(149);
				_la = _input.LA(1);
				if (_la==T__19) {
					{
					setState(148);
					match(T__19);
					}
				}

				setState(151);
				((MigrationStatementContext)_localctx).t = match(STRING);
				setState(152);
				((MigrationStatementContext)_localctx).createRuleset = createRuleset();
				 ((MigrationStatementContext)_localctx).rule =  new CreateRule(str((((MigrationStatementContext)_localctx).n!=null?((MigrationStatementContext)_localctx).n.getText():null)), str((((MigrationStatementContext)_localctx).t!=null?((MigrationStatementContext)_localctx).t.getText():null)), ((MigrationStatementContext)_localctx).createRuleset.ruleList); 
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(155);
				match(T__20);
				setState(156);
				((MigrationStatementContext)_localctx).conditions = conditions();
				setState(158);
				_la = _input.LA(1);
				if (_la==T__21) {
					{
					setState(157);
					match(T__21);
					}
				}

				setState(160);
				((MigrationStatementContext)_localctx).value = value();
				setState(161);
				match(T__1);
				 ((MigrationStatementContext)_localctx).rule =  new WarnRule(((MigrationStatementContext)_localctx).conditions.predicate, ((MigrationStatementContext)_localctx).value.valueProvider); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportingRuleContext extends ParserRuleContext {
		public ImportRuleListRule rule;
		public ConditionsContext conditions;
		public MigrationRulesetContext migrationRuleset;
		public WrapStatementsContext wrapStatements;
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public MigrationRulesetContext migrationRuleset() {
			return getRuleContext(MigrationRulesetContext.class,0);
		}
		public WrapStatementsContext wrapStatements() {
			return getRuleContext(WrapStatementsContext.class,0);
		}
		public ImportingRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importingRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterImportingRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitImportingRule(this);
		}
	}

	public final ImportingRuleContext importingRule() throws RecognitionException {
		ImportingRuleContext _localctx = new ImportingRuleContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_importingRule);
		try {
			setState(195);
			switch (_input.LA(1)) {
			case T__22:
				enterOuterAlt(_localctx, 1);
				{
				setState(166);
				match(T__22);
				setState(167);
				((ImportingRuleContext)_localctx).conditions = conditions();
				setState(173);
				switch (_input.LA(1)) {
				case T__4:
					{
					setState(168);
					((ImportingRuleContext)_localctx).migrationRuleset = migrationRuleset();
					 ((ImportingRuleContext)_localctx).rule =  new IncludeRule(((ImportingRuleContext)_localctx).conditions.predicate, ((ImportingRuleContext)_localctx).migrationRuleset.ruleList); 
					}
					break;
				case T__1:
					{
					setState(171);
					match(T__1);
					 ((ImportingRuleContext)_localctx).rule =  new IncludeRule(((ImportingRuleContext)_localctx).conditions.predicate, null); 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case T__23:
				enterOuterAlt(_localctx, 2);
				{
				setState(175);
				match(T__23);
				setState(176);
				((ImportingRuleContext)_localctx).conditions = conditions();
				setState(177);
				match(T__24);
				setState(178);
				((ImportingRuleContext)_localctx).wrapStatements = wrapStatements();
				setState(184);
				switch (_input.LA(1)) {
				case T__4:
					{
					setState(179);
					((ImportingRuleContext)_localctx).migrationRuleset = migrationRuleset();
					 ((ImportingRuleContext)_localctx).rule =  new WrapRule(((ImportingRuleContext)_localctx).conditions.predicate, ((ImportingRuleContext)_localctx).migrationRuleset.ruleList, ((ImportingRuleContext)_localctx).wrapStatements.weList); 
					}
					break;
				case T__1:
					{
					setState(182);
					match(T__1);
					 ((ImportingRuleContext)_localctx).rule =  new WrapRule(((ImportingRuleContext)_localctx).conditions.predicate, null, ((ImportingRuleContext)_localctx).wrapStatements.weList); 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case T__25:
				enterOuterAlt(_localctx, 3);
				{
				setState(186);
				match(T__25);
				setState(187);
				((ImportingRuleContext)_localctx).conditions = conditions();
				setState(193);
				switch (_input.LA(1)) {
				case T__4:
					{
					setState(188);
					((ImportingRuleContext)_localctx).migrationRuleset = migrationRuleset();
					 ((ImportingRuleContext)_localctx).rule =  new UnwrapRule(((ImportingRuleContext)_localctx).conditions.predicate, ((ImportingRuleContext)_localctx).migrationRuleset.ruleList); 
					}
					break;
				case T__1:
					{
					setState(191);
					match(T__1);
					 ((ImportingRuleContext)_localctx).rule =  new UnwrapRule(((ImportingRuleContext)_localctx).conditions.predicate, null); 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateStatementContext extends ParserRuleContext {
		public Rule rule;
		public Token STRING;
		public ValueContext value;
		public MapRulesetContext mapRuleset;
		public Token n;
		public Token t;
		public CreateRulesetContext createRuleset;
		public ConditionsContext conditions;
		public List<TerminalNode> STRING() { return getTokens(JCRMigrationEngineParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(JCRMigrationEngineParser.STRING, i);
		}
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public MapRulesetContext mapRuleset() {
			return getRuleContext(MapRulesetContext.class,0);
		}
		public CreateRulesetContext createRuleset() {
			return getRuleContext(CreateRulesetContext.class,0);
		}
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public CreateStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterCreateStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitCreateStatement(this);
		}
	}

	public final CreateStatementContext createStatement() throws RecognitionException {
		CreateStatementContext _localctx = new CreateStatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_createStatement);
		int _la;
		try {
			setState(236);
			switch (_input.LA(1)) {
			case T__16:
				enterOuterAlt(_localctx, 1);
				{
				setState(197);
				match(T__16);
				setState(199);
				_la = _input.LA(1);
				if (_la==T__11) {
					{
					setState(198);
					match(T__11);
					}
				}

				setState(201);
				((CreateStatementContext)_localctx).STRING = match(STRING);
				setState(202);
				match(T__17);
				setState(203);
				((CreateStatementContext)_localctx).value = value();
				setState(204);
				match(T__1);
				 ((CreateStatementContext)_localctx).rule =  RuleHelper.setPropertyRule(str((((CreateStatementContext)_localctx).STRING!=null?((CreateStatementContext)_localctx).STRING.getText():null)), ((CreateStatementContext)_localctx).value.valueProvider); 
				}
				break;
			case T__15:
				enterOuterAlt(_localctx, 2);
				{
				setState(207);
				match(T__15);
				setState(209);
				_la = _input.LA(1);
				if (_la==T__11) {
					{
					setState(208);
					match(T__11);
					}
				}

				setState(211);
				((CreateStatementContext)_localctx).STRING = match(STRING);
				setState(212);
				((CreateStatementContext)_localctx).mapRuleset = mapRuleset();
				 ((CreateStatementContext)_localctx).rule =  new MapRule(str((((CreateStatementContext)_localctx).STRING!=null?((CreateStatementContext)_localctx).STRING.getText():null)), ((CreateStatementContext)_localctx).mapRuleset.mapping, ((CreateStatementContext)_localctx).mapRuleset.hadDefault, ((CreateStatementContext)_localctx).mapRuleset.defaultMapping); 
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 3);
				{
				setState(215);
				match(T__18);
				setState(217);
				_la = _input.LA(1);
				if (_la==T__10) {
					{
					setState(216);
					match(T__10);
					}
				}

				setState(219);
				((CreateStatementContext)_localctx).n = match(STRING);
				setState(221);
				_la = _input.LA(1);
				if (_la==T__19) {
					{
					setState(220);
					match(T__19);
					}
				}

				setState(223);
				((CreateStatementContext)_localctx).t = match(STRING);
				setState(224);
				((CreateStatementContext)_localctx).createRuleset = createRuleset();
				 ((CreateStatementContext)_localctx).rule =  new CreateRule(str((((CreateStatementContext)_localctx).n!=null?((CreateStatementContext)_localctx).n.getText():null)), str((((CreateStatementContext)_localctx).t!=null?((CreateStatementContext)_localctx).t.getText():null)), ((CreateStatementContext)_localctx).createRuleset.ruleList); 
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 4);
				{
				setState(227);
				match(T__14);
				setState(228);
				((CreateStatementContext)_localctx).conditions = conditions();
				setState(229);
				((CreateStatementContext)_localctx).createRuleset = createRuleset();
				 ((CreateStatementContext)_localctx).rule =  new IfRule(((CreateStatementContext)_localctx).conditions.predicate, ((CreateStatementContext)_localctx).createRuleset.ruleList); 
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 5);
				{
				setState(232);
				match(T__12);
				setState(233);
				((CreateStatementContext)_localctx).STRING = match(STRING);
				setState(234);
				match(T__1);
				 ((CreateStatementContext)_localctx).rule =  new UseRule(str((((CreateStatementContext)_localctx).STRING!=null?((CreateStatementContext)_localctx).STRING.getText():null))); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateRulesetContext extends ParserRuleContext {
		public RuleList ruleList;
		public CreateStatementContext it;
		public List<CreateStatementContext> createStatement() {
			return getRuleContexts(CreateStatementContext.class);
		}
		public CreateStatementContext createStatement(int i) {
			return getRuleContext(CreateStatementContext.class,i);
		}
		public CreateRulesetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createRuleset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterCreateRuleset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitCreateRuleset(this);
		}
	}

	public final CreateRulesetContext createRuleset() throws RecognitionException {
		CreateRulesetContext _localctx = new CreateRulesetContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_createRuleset);
		 ((CreateRulesetContext)_localctx).ruleList =  new RuleList(); 
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			match(T__4);
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__18))) != 0)) {
				{
				{
				setState(239);
				((CreateRulesetContext)_localctx).it = createStatement();
				 _localctx.ruleList.addRule(((CreateRulesetContext)_localctx).it.rule); 
				}
				}
				setState(246);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(247);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WrapStatementsContext extends ParserRuleContext {
		public List<WrapRule.WrappingNode> weList;
		public WrapStatementContext wrapStatement;
		public List<WrapStatementContext> wrapStatement() {
			return getRuleContexts(WrapStatementContext.class);
		}
		public WrapStatementContext wrapStatement(int i) {
			return getRuleContext(WrapStatementContext.class,i);
		}
		public WrapStatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wrapStatements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterWrapStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitWrapStatements(this);
		}
	}

	public final WrapStatementsContext wrapStatements() throws RecognitionException {
		WrapStatementsContext _localctx = new WrapStatementsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_wrapStatements);
		 ((WrapStatementsContext)_localctx).weList = new ArrayList<WrapRule.WrappingNode>(); 
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249);
			((WrapStatementsContext)_localctx).wrapStatement = wrapStatement();
			 _localctx.weList.add(((WrapStatementsContext)_localctx).wrapStatement.we); 
			setState(257);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(251);
				match(T__26);
				setState(252);
				((WrapStatementsContext)_localctx).wrapStatement = wrapStatement();
				 _localctx.weList.add(((WrapStatementsContext)_localctx).wrapStatement.we); 
				}
				}
				setState(259);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WrapStatementContext extends ParserRuleContext {
		public WrapRule.WrappingNode we;
		public Token n;
		public Token t;
		public List<TerminalNode> STRING() { return getTokens(JCRMigrationEngineParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(JCRMigrationEngineParser.STRING, i);
		}
		public WrapStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wrapStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterWrapStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitWrapStatement(this);
		}
	}

	public final WrapStatementContext wrapStatement() throws RecognitionException {
		WrapStatementContext _localctx = new WrapStatementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_wrapStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			_la = _input.LA(1);
			if (_la==T__10) {
				{
				setState(260);
				match(T__10);
				}
			}

			setState(263);
			((WrapStatementContext)_localctx).n = match(STRING);
			setState(265);
			_la = _input.LA(1);
			if (_la==T__19) {
				{
				setState(264);
				match(T__19);
				}
			}

			setState(267);
			((WrapStatementContext)_localctx).t = match(STRING);
			 ((WrapStatementContext)_localctx).we =  new WrapRule.WrappingNode(str((((WrapStatementContext)_localctx).n!=null?((WrapStatementContext)_localctx).n.getText():null)), str((((WrapStatementContext)_localctx).t!=null?((WrapStatementContext)_localctx).t.getText():null))); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderSpecContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(JCRMigrationEngineParser.STRING, 0); }
		public OrderSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderSpec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterOrderSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitOrderSpec(this);
		}
	}

	public final OrderSpecContext orderSpec() throws RecognitionException {
		OrderSpecContext _localctx = new OrderSpecContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_orderSpec);
		try {
			setState(276);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(270);
				match(T__27);
				setState(271);
				match(T__28);
				setState(272);
				match(STRING);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(273);
				match(T__27);
				setState(274);
				match(T__29);
				setState(275);
				match(STRING);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MapRulesetContext extends ParserRuleContext {
		public Map<ValueKey,Value> mapping;
		public Value defaultMapping;
		public boolean hadDefault;
		public LiteralValueContext literalValue;
		public List<MapStatementContext> mapStatement() {
			return getRuleContexts(MapStatementContext.class);
		}
		public MapStatementContext mapStatement(int i) {
			return getRuleContext(MapStatementContext.class,i);
		}
		public LiteralValueContext literalValue() {
			return getRuleContext(LiteralValueContext.class,0);
		}
		public MapRulesetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mapRuleset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterMapRuleset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitMapRuleset(this);
		}
	}

	public final MapRulesetContext mapRuleset() throws RecognitionException {
		MapRulesetContext _localctx = new MapRulesetContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_mapRuleset);
		 ((MapRulesetContext)_localctx).mapping =  new LinkedHashMap<ValueKey,Value>(); ((MapRulesetContext)_localctx).hadDefault = false; ((MapRulesetContext)_localctx).defaultMapping = null; 
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(278);
			match(T__4);
			setState(279);
			mapStatement(_localctx.mapping);
			setState(284);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(280);
					match(T__26);
					setState(281);
					mapStatement(_localctx.mapping);
					}
					} 
				}
				setState(286);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			setState(293);
			_la = _input.LA(1);
			if (_la==T__26) {
				{
				setState(287);
				match(T__26);
				setState(288);
				match(T__30);
				setState(289);
				match(T__3);
				setState(290);
				((MapRulesetContext)_localctx).literalValue = literalValue();
				 ((MapRulesetContext)_localctx).hadDefault = true; ((MapRulesetContext)_localctx).defaultMapping = ((MapRulesetContext)_localctx).literalValue.jcrValue; 
				}
			}

			setState(295);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MapStatementContext extends ParserRuleContext {
		public Map<ValueKey,Value> mapping;
		public LiteralValueContext f;
		public LiteralValueContext t;
		public List<LiteralValueContext> literalValue() {
			return getRuleContexts(LiteralValueContext.class);
		}
		public LiteralValueContext literalValue(int i) {
			return getRuleContext(LiteralValueContext.class,i);
		}
		public MapStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MapStatementContext(ParserRuleContext parent, int invokingState, Map<ValueKey,Value> mapping) {
			super(parent, invokingState);
			this.mapping = mapping;
		}
		@Override public int getRuleIndex() { return RULE_mapStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterMapStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitMapStatement(this);
		}
	}

	public final MapStatementContext mapStatement(Map<ValueKey,Value> mapping) throws RecognitionException {
		MapStatementContext _localctx = new MapStatementContext(_ctx, getState(), mapping);
		enterRule(_localctx, 22, RULE_mapStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			((MapStatementContext)_localctx).f = literalValue();
			setState(298);
			match(T__3);
			setState(299);
			((MapStatementContext)_localctx).t = literalValue();
			 _localctx.mapping.put(new ValueKey(((MapStatementContext)_localctx).f.jcrValue),((MapStatementContext)_localctx).t.jcrValue); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionsContext extends ParserRuleContext {
		public Predicate predicate;
		public ConditionContext condition;
		public ConditionsContext c;
		public ConditionsTailContext conditionsTail;
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public ConditionsTailContext conditionsTail() {
			return getRuleContext(ConditionsTailContext.class,0);
		}
		public ConditionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterConditions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitConditions(this);
		}
	}

	public final ConditionsContext conditions() throws RecognitionException {
		ConditionsContext _localctx = new ConditionsContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_conditions);
		int _la;
		try {
			setState(318);
			switch (_input.LA(1)) {
			case T__31:
				enterOuterAlt(_localctx, 1);
				{
				setState(302);
				match(T__31);
				setState(303);
				((ConditionsContext)_localctx).condition = condition();
				 ((ConditionsContext)_localctx).predicate =  PredicateHelper.negate(((ConditionsContext)_localctx).condition.predicate); 
				}
				break;
			case T__32:
				enterOuterAlt(_localctx, 2);
				{
				setState(306);
				match(T__32);
				setState(307);
				((ConditionsContext)_localctx).c = conditions();
				setState(308);
				match(T__33);
				 ((ConditionsContext)_localctx).predicate =  ((ConditionsContext)_localctx).c.predicate; 
				}
				break;
			case T__10:
			case T__11:
			case T__19:
			case T__37:
			case T__38:
			case T__39:
			case T__41:
				enterOuterAlt(_localctx, 3);
				{
				setState(311);
				((ConditionsContext)_localctx).condition = condition();
				 ((ConditionsContext)_localctx).predicate =  ((ConditionsContext)_localctx).condition.predicate; 
				setState(316);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__34) | (1L << T__35) | (1L << T__36))) != 0)) {
					{
					setState(313);
					((ConditionsContext)_localctx).conditionsTail = conditionsTail(((ConditionsContext)_localctx).condition.predicate);
					 ((ConditionsContext)_localctx).predicate =  ((ConditionsContext)_localctx).conditionsTail.predicate; 
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionsTailContext extends ParserRuleContext {
		public Predicate leftSide;
		public Predicate predicate;
		public ConditionsContext conditions;
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public ConditionsTailContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConditionsTailContext(ParserRuleContext parent, int invokingState, Predicate leftSide) {
			super(parent, invokingState);
			this.leftSide = leftSide;
		}
		@Override public int getRuleIndex() { return RULE_conditionsTail; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterConditionsTail(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitConditionsTail(this);
		}
	}

	public final ConditionsTailContext conditionsTail(Predicate leftSide) throws RecognitionException {
		ConditionsTailContext _localctx = new ConditionsTailContext(_ctx, getState(), leftSide);
		enterRule(_localctx, 26, RULE_conditionsTail);
		try {
			setState(332);
			switch (_input.LA(1)) {
			case T__34:
				enterOuterAlt(_localctx, 1);
				{
				setState(320);
				match(T__34);
				setState(321);
				((ConditionsTailContext)_localctx).conditions = conditions();
				 ((ConditionsTailContext)_localctx).predicate =  PredicateHelper.conjunction(_localctx.leftSide, ((ConditionsTailContext)_localctx).conditions.predicate); 
				}
				break;
			case T__35:
				enterOuterAlt(_localctx, 2);
				{
				setState(324);
				match(T__35);
				setState(325);
				((ConditionsTailContext)_localctx).conditions = conditions();
				 ((ConditionsTailContext)_localctx).predicate =  PredicateHelper.disjunction(_localctx.leftSide, ((ConditionsTailContext)_localctx).conditions.predicate); 
				}
				break;
			case T__36:
				enterOuterAlt(_localctx, 3);
				{
				setState(328);
				match(T__36);
				setState(329);
				((ConditionsTailContext)_localctx).conditions = conditions();
				 ((ConditionsTailContext)_localctx).predicate =  PredicateHelper.xor(_localctx.leftSide, ((ConditionsTailContext)_localctx).conditions.predicate); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public Predicate predicate;
		public Token STRING;
		public Token n;
		public Token t;
		public ValueComparisonContext v;
		public Token LONG;
		public Token d1;
		public Token d2;
		public List<TerminalNode> STRING() { return getTokens(JCRMigrationEngineParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(JCRMigrationEngineParser.STRING, i);
		}
		public ValueComparisonContext valueComparison() {
			return getRuleContext(ValueComparisonContext.class,0);
		}
		public List<TerminalNode> LONG() { return getTokens(JCRMigrationEngineParser.LONG); }
		public TerminalNode LONG(int i) {
			return getToken(JCRMigrationEngineParser.LONG, i);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_condition);
		int _la;
		try {
			setState(380);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(334);
				match(T__19);
				setState(335);
				((ConditionContext)_localctx).STRING = match(STRING);
				 ((ConditionContext)_localctx).predicate =  new NodeTypePredicate(str((((ConditionContext)_localctx).STRING!=null?((ConditionContext)_localctx).STRING.getText():null))); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(337);
				match(T__10);
				setState(338);
				((ConditionContext)_localctx).n = match(STRING);
				setState(341);
				_la = _input.LA(1);
				if (_la==T__19) {
					{
					setState(339);
					match(T__19);
					setState(340);
					((ConditionContext)_localctx).t = match(STRING);
					}
				}

				 ((ConditionContext)_localctx).predicate =  PredicateHelper.nodeNamePredicate(str((((ConditionContext)_localctx).n!=null?((ConditionContext)_localctx).n.getText():null)),str((((ConditionContext)_localctx).t!=null?((ConditionContext)_localctx).t.getText():null))); 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(344);
				match(T__37);
				setState(345);
				match(T__10);
				setState(346);
				((ConditionContext)_localctx).n = match(STRING);
				setState(349);
				_la = _input.LA(1);
				if (_la==T__19) {
					{
					setState(347);
					match(T__19);
					setState(348);
					((ConditionContext)_localctx).t = match(STRING);
					}
				}

				 ((ConditionContext)_localctx).predicate =  PredicateHelper.withNodePredicate(str((((ConditionContext)_localctx).n!=null?((ConditionContext)_localctx).n.getText():null)),str((((ConditionContext)_localctx).t!=null?((ConditionContext)_localctx).t.getText():null))); 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(352);
				match(T__37);
				setState(354);
				_la = _input.LA(1);
				if (_la==T__11) {
					{
					setState(353);
					match(T__11);
					}
				}

				setState(356);
				((ConditionContext)_localctx).STRING = match(STRING);
				setState(358);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46) | (1L << T__47) | (1L << T__48))) != 0)) {
					{
					setState(357);
					((ConditionContext)_localctx).v = valueComparison();
					}
				}

				 ((ConditionContext)_localctx).predicate =  PredicateHelper.withPropertyPredicate(str((((ConditionContext)_localctx).STRING!=null?((ConditionContext)_localctx).STRING.getText():null)), (((ConditionContext)_localctx).v!=null)?((ConditionContext)_localctx).v.operator:null, (((ConditionContext)_localctx).v!=null)?((ConditionContext)_localctx).v.valueProvider:null); 
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(361);
				match(T__11);
				setState(362);
				((ConditionContext)_localctx).STRING = match(STRING);
				setState(364);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46) | (1L << T__47) | (1L << T__48))) != 0)) {
					{
					setState(363);
					((ConditionContext)_localctx).v = valueComparison();
					}
				}

				 ((ConditionContext)_localctx).predicate =  PredicateHelper.propertyPredicate(str((((ConditionContext)_localctx).STRING!=null?((ConditionContext)_localctx).STRING.getText():null)), (((ConditionContext)_localctx).v!=null)?((ConditionContext)_localctx).v.operator:null, (((ConditionContext)_localctx).v!=null)?((ConditionContext)_localctx).v.valueProvider:null); 
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(367);
				match(T__38);
				setState(368);
				((ConditionContext)_localctx).LONG = match(LONG);
				 ((ConditionContext)_localctx).predicate =  new NodePositionPredicate(Long.valueOf((((ConditionContext)_localctx).LONG!=null?((ConditionContext)_localctx).LONG.getText():null))); 
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(370);
				match(T__39);
				setState(371);
				((ConditionContext)_localctx).d1 = match(LONG);
				setState(374);
				_la = _input.LA(1);
				if (_la==T__40) {
					{
					setState(372);
					match(T__40);
					setState(373);
					((ConditionContext)_localctx).d2 = match(LONG);
					}
				}

				 ((ConditionContext)_localctx).predicate =  PredicateHelper.depthPredicate(Long.valueOf((((ConditionContext)_localctx).d1!=null?((ConditionContext)_localctx).d1.getText():null)), ((((ConditionContext)_localctx).d2!=null?((ConditionContext)_localctx).d2.getText():null)!=null)?Long.valueOf((((ConditionContext)_localctx).d2!=null?((ConditionContext)_localctx).d2.getText():null)):null); 
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(377);
				match(T__41);
				setState(378);
				((ConditionContext)_localctx).STRING = match(STRING);
				 ((ConditionContext)_localctx).predicate =  new WorkspaceNamePredicate(str((((ConditionContext)_localctx).STRING!=null?((ConditionContext)_localctx).STRING.getText():null))); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueComparisonContext extends ParserRuleContext {
		public Operator operator;
		public ValueProvider valueProvider;
		public ValueContext value;
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ValueComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueComparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterValueComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitValueComparison(this);
		}
	}

	public final ValueComparisonContext valueComparison() throws RecognitionException {
		ValueComparisonContext _localctx = new ValueComparisonContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_valueComparison);
		try {
			setState(410);
			switch (_input.LA(1)) {
			case T__42:
				enterOuterAlt(_localctx, 1);
				{
				setState(382);
				match(T__42);
				setState(383);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.EQUAL; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			case T__43:
				enterOuterAlt(_localctx, 2);
				{
				setState(386);
				match(T__43);
				setState(387);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.GT; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			case T__44:
				enterOuterAlt(_localctx, 3);
				{
				setState(390);
				match(T__44);
				setState(391);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.LT; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			case T__45:
				enterOuterAlt(_localctx, 4);
				{
				setState(394);
				match(T__45);
				setState(395);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.GT_EQ; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			case T__46:
				enterOuterAlt(_localctx, 5);
				{
				setState(398);
				match(T__46);
				setState(399);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.LT_EQ; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			case T__47:
				enterOuterAlt(_localctx, 6);
				{
				setState(402);
				match(T__47);
				setState(403);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.NOTEQUAL; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			case T__48:
				enterOuterAlt(_localctx, 7);
				{
				setState(406);
				match(T__48);
				setState(407);
				((ValueComparisonContext)_localctx).value = value();
				 ((ValueComparisonContext)_localctx).operator = Operator.REGEX; ((ValueComparisonContext)_localctx).valueProvider = ((ValueComparisonContext)_localctx).value.valueProvider; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueProvider valueProvider;
		public ValueSpecContext valueSpec;
		public CastSpecContext castSpec;
		public ValueSpecContext valueSpec() {
			return getRuleContext(ValueSpecContext.class,0);
		}
		public CastSpecContext castSpec() {
			return getRuleContext(CastSpecContext.class,0);
		}
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_value);
		try {
			setState(421);
			switch (_input.LA(1)) {
			case T__11:
			case T__55:
			case BOOL:
			case LONG:
			case DOUBLE:
			case STRING:
			case FUNCNAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(412);
				((ValueContext)_localctx).valueSpec = valueSpec();
				 ((ValueContext)_localctx).valueProvider =  ((ValueContext)_localctx).valueSpec.valueProvider; 
				}
				break;
			case T__32:
				enterOuterAlt(_localctx, 2);
				{
				setState(415);
				match(T__32);
				setState(416);
				((ValueContext)_localctx).castSpec = castSpec();
				setState(417);
				match(T__33);
				setState(418);
				((ValueContext)_localctx).valueSpec = valueSpec();
				 ((ValueContext)_localctx).valueProvider =  new CastingValueProvider((((ValueContext)_localctx).castSpec!=null?_input.getText(((ValueContext)_localctx).castSpec.start,((ValueContext)_localctx).castSpec.stop):null), ((ValueContext)_localctx).valueSpec.valueProvider); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastSpecContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(JCRMigrationEngineParser.STRING, 0); }
		public CastSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_castSpec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterCastSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitCastSpec(this);
		}
	}

	public final CastSpecContext castSpec() throws RecognitionException {
		CastSpecContext _localctx = new CastSpecContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_castSpec);
		int _la;
		try {
			setState(432);
			switch (_input.LA(1)) {
			case T__49:
				enterOuterAlt(_localctx, 1);
				{
				setState(423);
				match(T__49);
				setState(425);
				_la = _input.LA(1);
				if (_la==STRING) {
					{
					setState(424);
					match(STRING);
					}
				}

				}
				break;
			case T__50:
				enterOuterAlt(_localctx, 2);
				{
				setState(427);
				match(T__50);
				}
				break;
			case T__51:
				enterOuterAlt(_localctx, 3);
				{
				setState(428);
				match(T__51);
				}
				break;
			case T__52:
				enterOuterAlt(_localctx, 4);
				{
				setState(429);
				match(T__52);
				}
				break;
			case T__53:
				enterOuterAlt(_localctx, 5);
				{
				setState(430);
				match(T__53);
				}
				break;
			case T__54:
				enterOuterAlt(_localctx, 6);
				{
				setState(431);
				match(T__54);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueSpecContext extends ParserRuleContext {
		public ValueProvider valueProvider;
		public LiteralValueContext literalValue;
		public Token STRING;
		public Token FUNCNAME;
		public ValueListContext valueList;
		public LiteralValueContext literalValue() {
			return getRuleContext(LiteralValueContext.class,0);
		}
		public TerminalNode STRING() { return getToken(JCRMigrationEngineParser.STRING, 0); }
		public TerminalNode FUNCNAME() { return getToken(JCRMigrationEngineParser.FUNCNAME, 0); }
		public ValueListContext valueList() {
			return getRuleContext(ValueListContext.class,0);
		}
		public ValueSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueSpec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterValueSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitValueSpec(this);
		}
	}

	public final ValueSpecContext valueSpec() throws RecognitionException {
		ValueSpecContext _localctx = new ValueSpecContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_valueSpec);
		try {
			setState(446);
			switch (_input.LA(1)) {
			case T__55:
			case BOOL:
			case LONG:
			case DOUBLE:
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(434);
				((ValueSpecContext)_localctx).literalValue = literalValue();
				 ((ValueSpecContext)_localctx).valueProvider =  new LiteralValueProvider(((ValueSpecContext)_localctx).literalValue.jcrValue); 
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 2);
				{
				setState(437);
				match(T__11);
				setState(438);
				((ValueSpecContext)_localctx).STRING = match(STRING);
				 ((ValueSpecContext)_localctx).valueProvider =  new PropertyValueProvider(str((((ValueSpecContext)_localctx).STRING!=null?((ValueSpecContext)_localctx).STRING.getText():null))); 
				}
				break;
			case FUNCNAME:
				enterOuterAlt(_localctx, 3);
				{
				setState(440);
				((ValueSpecContext)_localctx).FUNCNAME = match(FUNCNAME);
				setState(441);
				match(T__32);
				setState(442);
				((ValueSpecContext)_localctx).valueList = valueList();
				setState(443);
				match(T__33);
				 ((ValueSpecContext)_localctx).valueProvider =  ValueHelper.function((((ValueSpecContext)_localctx).FUNCNAME!=null?((ValueSpecContext)_localctx).FUNCNAME.getText():null), ((ValueSpecContext)_localctx).valueList.valueProviders ); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueListContext extends ParserRuleContext {
		public List<ValueProvider> valueProviders;
		public ValueContext v1;
		public ValueContext v2;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public ValueListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterValueList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitValueList(this);
		}
	}

	public final ValueListContext valueList() throws RecognitionException {
		ValueListContext _localctx = new ValueListContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_valueList);
		 ((ValueListContext)_localctx).valueProviders =  new ArrayList<ValueProvider>(); 
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(459);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__32) | (1L << T__55) | (1L << BOOL) | (1L << LONG) | (1L << DOUBLE) | (1L << STRING) | (1L << FUNCNAME))) != 0)) {
				{
				setState(448);
				((ValueListContext)_localctx).v1 = value();
				 _localctx.valueProviders.add(((ValueListContext)_localctx).v1.valueProvider); 
				setState(456);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__26) {
					{
					{
					setState(450);
					match(T__26);
					setState(451);
					((ValueListContext)_localctx).v2 = value();
					 _localctx.valueProviders.add(((ValueListContext)_localctx).v2.valueProvider); 
					}
					}
					setState(458);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralValueContext extends ParserRuleContext {
		public Value jcrValue;
		public Token STRING;
		public Token LONG;
		public Token BOOL;
		public Token DOUBLE;
		public TerminalNode STRING() { return getToken(JCRMigrationEngineParser.STRING, 0); }
		public TerminalNode LONG() { return getToken(JCRMigrationEngineParser.LONG, 0); }
		public TerminalNode BOOL() { return getToken(JCRMigrationEngineParser.BOOL, 0); }
		public TerminalNode DOUBLE() { return getToken(JCRMigrationEngineParser.DOUBLE, 0); }
		public LiteralValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).enterLiteralValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JCRMigrationEngineListener ) ((JCRMigrationEngineListener)listener).exitLiteralValue(this);
		}
	}

	public final LiteralValueContext literalValue() throws RecognitionException {
		LiteralValueContext _localctx = new LiteralValueContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_literalValue);
		try {
			setState(471);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(461);
				((LiteralValueContext)_localctx).STRING = match(STRING);
				 ((LiteralValueContext)_localctx).jcrValue =  ValueHelper.stringValue(str((((LiteralValueContext)_localctx).STRING!=null?((LiteralValueContext)_localctx).STRING.getText():null))); 
				}
				break;
			case LONG:
				enterOuterAlt(_localctx, 2);
				{
				setState(463);
				((LiteralValueContext)_localctx).LONG = match(LONG);
				 ((LiteralValueContext)_localctx).jcrValue =  ValueHelper.longValue((((LiteralValueContext)_localctx).LONG!=null?((LiteralValueContext)_localctx).LONG.getText():null)); 
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 3);
				{
				setState(465);
				((LiteralValueContext)_localctx).BOOL = match(BOOL);
				 ((LiteralValueContext)_localctx).jcrValue =  ValueHelper.booleanValue((((LiteralValueContext)_localctx).BOOL!=null?((LiteralValueContext)_localctx).BOOL.getText():null)); 
				}
				break;
			case DOUBLE:
				enterOuterAlt(_localctx, 4);
				{
				setState(467);
				((LiteralValueContext)_localctx).DOUBLE = match(DOUBLE);
				 ((LiteralValueContext)_localctx).jcrValue =  ValueHelper.doubleValue((((LiteralValueContext)_localctx).DOUBLE!=null?((LiteralValueContext)_localctx).DOUBLE.getText():null)); 
				}
				break;
			case T__55:
				enterOuterAlt(_localctx, 5);
				{
				setState(469);
				match(T__55);
				 ((LiteralValueContext)_localctx).jcrValue =  null; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3B\u01dc\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\2\3\2\3\2\7\2\62\n"+
		"\2\f\2\16\2\65\13\2\3\2\3\2\3\2\7\2:\n\2\f\2\16\2=\13\2\3\2\3\2\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\7\4L\n\4\f\4\16\4O\13\4\3\4\3\4"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u0082\n\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\5\5\u008a\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u0094"+
		"\n\5\3\5\3\5\5\5\u0098\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u00a1\n\5\3"+
		"\5\3\5\3\5\3\5\5\5\u00a7\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00b0\n\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00bb\n\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\5\6\u00c4\n\6\5\6\u00c6\n\6\3\7\3\7\5\7\u00ca\n\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\5\7\u00d4\n\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00dc\n"+
		"\7\3\7\3\7\5\7\u00e0\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\5\7\u00ef\n\7\3\b\3\b\3\b\3\b\7\b\u00f5\n\b\f\b\16\b\u00f8\13\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u0102\n\t\f\t\16\t\u0105\13\t\3\n"+
		"\5\n\u0108\n\n\3\n\3\n\5\n\u010c\n\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\5\13\u0117\n\13\3\f\3\f\3\f\3\f\7\f\u011d\n\f\f\f\16\f\u0120"+
		"\13\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0128\n\f\3\f\3\f\3\r\3\r\3\r\3\r\3"+
		"\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3"+
		"\16\5\16\u013f\n\16\5\16\u0141\n\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\5\17\u014f\n\17\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\5\20\u0158\n\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u0160\n\20\3"+
		"\20\3\20\3\20\5\20\u0165\n\20\3\20\3\20\5\20\u0169\n\20\3\20\3\20\3\20"+
		"\3\20\5\20\u016f\n\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u0179"+
		"\n\20\3\20\3\20\3\20\3\20\5\20\u017f\n\20\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u019d\n\21\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u01a8\n\22\3\23\3\23\5\23\u01ac\n"+
		"\23\3\23\3\23\3\23\3\23\3\23\5\23\u01b3\n\23\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u01c1\n\24\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\7\25\u01c9\n\25\f\25\16\25\u01cc\13\25\5\25\u01ce\n\25\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u01da\n\26\3\26\2\2"+
		"\27\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*\2\2\u0215\2,\3\2\2\2"+
		"\4@\3\2\2\2\6G\3\2\2\2\b\u00a6\3\2\2\2\n\u00c5\3\2\2\2\f\u00ee\3\2\2\2"+
		"\16\u00f0\3\2\2\2\20\u00fb\3\2\2\2\22\u0107\3\2\2\2\24\u0116\3\2\2\2\26"+
		"\u0118\3\2\2\2\30\u012b\3\2\2\2\32\u0140\3\2\2\2\34\u014e\3\2\2\2\36\u017e"+
		"\3\2\2\2 \u019c\3\2\2\2\"\u01a7\3\2\2\2$\u01b2\3\2\2\2&\u01c0\3\2\2\2"+
		"(\u01cd\3\2\2\2*\u01d9\3\2\2\2,-\7\3\2\2-.\7?\2\2./\b\2\1\2/\63\7\4\2"+
		"\2\60\62\5\4\3\2\61\60\3\2\2\2\62\65\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2"+
		"\2\64;\3\2\2\2\65\63\3\2\2\2\66\67\5\b\5\2\678\b\2\1\28:\3\2\2\29\66\3"+
		"\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<>\3\2\2\2=;\3\2\2\2>?\7\2\2\3?\3"+
		"\3\2\2\2@A\7\5\2\2AB\7?\2\2BC\7\6\2\2CD\7?\2\2DE\7\4\2\2EF\b\3\1\2F\5"+
		"\3\2\2\2GM\7\7\2\2HI\5\b\5\2IJ\b\4\1\2JL\3\2\2\2KH\3\2\2\2LO\3\2\2\2M"+
		"K\3\2\2\2MN\3\2\2\2NP\3\2\2\2OM\3\2\2\2PQ\7\b\2\2Q\7\3\2\2\2RS\7\t\2\2"+
		"ST\5\32\16\2TU\7\4\2\2UV\b\5\1\2V\u00a7\3\2\2\2WX\7\n\2\2XY\5\32\16\2"+
		"YZ\7\4\2\2Z\u00a7\3\2\2\2[\\\7\13\2\2\\]\7?\2\2]^\5\6\4\2^_\b\5\1\2_\u00a7"+
		"\3\2\2\2`a\7\f\2\2ab\7\r\2\2bc\7?\2\2cd\7\6\2\2de\5\"\22\2ef\7\4\2\2f"+
		"g\b\5\1\2g\u00a7\3\2\2\2hi\7\f\2\2ij\7\16\2\2jk\7?\2\2kl\7\6\2\2lm\7?"+
		"\2\2mn\7\4\2\2n\u00a7\b\5\1\2op\7\17\2\2pq\7?\2\2qr\7\4\2\2r\u00a7\b\5"+
		"\1\2st\5\n\6\2tu\b\5\1\2u\u00a7\3\2\2\2vw\7\20\2\2wx\5\n\6\2xy\b\5\1\2"+
		"y\u00a7\3\2\2\2z{\7\21\2\2{|\5\32\16\2|}\5\6\4\2}~\b\5\1\2~\u00a7\3\2"+
		"\2\2\177\u0081\7\22\2\2\u0080\u0082\7\16\2\2\u0081\u0080\3\2\2\2\u0081"+
		"\u0082\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\7?\2\2\u0084\u0085\5\26"+
		"\f\2\u0085\u0086\b\5\1\2\u0086\u00a7\3\2\2\2\u0087\u0089\7\23\2\2\u0088"+
		"\u008a\7\16\2\2\u0089\u0088\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008b\3"+
		"\2\2\2\u008b\u008c\7?\2\2\u008c\u008d\7\24\2\2\u008d\u008e\5\"\22\2\u008e"+
		"\u008f\7\4\2\2\u008f\u0090\b\5\1\2\u0090\u00a7\3\2\2\2\u0091\u0093\7\25"+
		"\2\2\u0092\u0094\7\r\2\2\u0093\u0092\3\2\2\2\u0093\u0094\3\2\2\2\u0094"+
		"\u0095\3\2\2\2\u0095\u0097\7?\2\2\u0096\u0098\7\26\2\2\u0097\u0096\3\2"+
		"\2\2\u0097\u0098\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009a\7?\2\2\u009a"+
		"\u009b\5\16\b\2\u009b\u009c\b\5\1\2\u009c\u00a7\3\2\2\2\u009d\u009e\7"+
		"\27\2\2\u009e\u00a0\5\32\16\2\u009f\u00a1\7\30\2\2\u00a0\u009f\3\2\2\2"+
		"\u00a0\u00a1\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a3\5\"\22\2\u00a3\u00a4"+
		"\7\4\2\2\u00a4\u00a5\b\5\1\2\u00a5\u00a7\3\2\2\2\u00a6R\3\2\2\2\u00a6"+
		"W\3\2\2\2\u00a6[\3\2\2\2\u00a6`\3\2\2\2\u00a6h\3\2\2\2\u00a6o\3\2\2\2"+
		"\u00a6s\3\2\2\2\u00a6v\3\2\2\2\u00a6z\3\2\2\2\u00a6\177\3\2\2\2\u00a6"+
		"\u0087\3\2\2\2\u00a6\u0091\3\2\2\2\u00a6\u009d\3\2\2\2\u00a7\t\3\2\2\2"+
		"\u00a8\u00a9\7\31\2\2\u00a9\u00af\5\32\16\2\u00aa\u00ab\5\6\4\2\u00ab"+
		"\u00ac\b\6\1\2\u00ac\u00b0\3\2\2\2\u00ad\u00ae\7\4\2\2\u00ae\u00b0\b\6"+
		"\1\2\u00af\u00aa\3\2\2\2\u00af\u00ad\3\2\2\2\u00b0\u00c6\3\2\2\2\u00b1"+
		"\u00b2\7\32\2\2\u00b2\u00b3\5\32\16\2\u00b3\u00b4\7\33\2\2\u00b4\u00ba"+
		"\5\20\t\2\u00b5\u00b6\5\6\4\2\u00b6\u00b7\b\6\1\2\u00b7\u00bb\3\2\2\2"+
		"\u00b8\u00b9\7\4\2\2\u00b9\u00bb\b\6\1\2\u00ba\u00b5\3\2\2\2\u00ba\u00b8"+
		"\3\2\2\2\u00bb\u00c6\3\2\2\2\u00bc\u00bd\7\34\2\2\u00bd\u00c3\5\32\16"+
		"\2\u00be\u00bf\5\6\4\2\u00bf\u00c0\b\6\1\2\u00c0\u00c4\3\2\2\2\u00c1\u00c2"+
		"\7\4\2\2\u00c2\u00c4\b\6\1\2\u00c3\u00be\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c4"+
		"\u00c6\3\2\2\2\u00c5\u00a8\3\2\2\2\u00c5\u00b1\3\2\2\2\u00c5\u00bc\3\2"+
		"\2\2\u00c6\13\3\2\2\2\u00c7\u00c9\7\23\2\2\u00c8\u00ca\7\16\2\2\u00c9"+
		"\u00c8\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb\u00cc\7?"+
		"\2\2\u00cc\u00cd\7\24\2\2\u00cd\u00ce\5\"\22\2\u00ce\u00cf\7\4\2\2\u00cf"+
		"\u00d0\b\7\1\2\u00d0\u00ef\3\2\2\2\u00d1\u00d3\7\22\2\2\u00d2\u00d4\7"+
		"\16\2\2\u00d3\u00d2\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5"+
		"\u00d6\7?\2\2\u00d6\u00d7\5\26\f\2\u00d7\u00d8\b\7\1\2\u00d8\u00ef\3\2"+
		"\2\2\u00d9\u00db\7\25\2\2\u00da\u00dc\7\r\2\2\u00db\u00da\3\2\2\2\u00db"+
		"\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00df\7?\2\2\u00de\u00e0\7\26"+
		"\2\2\u00df\u00de\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1"+
		"\u00e2\7?\2\2\u00e2\u00e3\5\16\b\2\u00e3\u00e4\b\7\1\2\u00e4\u00ef\3\2"+
		"\2\2\u00e5\u00e6\7\21\2\2\u00e6\u00e7\5\32\16\2\u00e7\u00e8\5\16\b\2\u00e8"+
		"\u00e9\b\7\1\2\u00e9\u00ef\3\2\2\2\u00ea\u00eb\7\17\2\2\u00eb\u00ec\7"+
		"?\2\2\u00ec\u00ed\7\4\2\2\u00ed\u00ef\b\7\1\2\u00ee\u00c7\3\2\2\2\u00ee"+
		"\u00d1\3\2\2\2\u00ee\u00d9\3\2\2\2\u00ee\u00e5\3\2\2\2\u00ee\u00ea\3\2"+
		"\2\2\u00ef\r\3\2\2\2\u00f0\u00f6\7\7\2\2\u00f1\u00f2\5\f\7\2\u00f2\u00f3"+
		"\b\b\1\2\u00f3\u00f5\3\2\2\2\u00f4\u00f1\3\2\2\2\u00f5\u00f8\3\2\2\2\u00f6"+
		"\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f9\3\2\2\2\u00f8\u00f6\3\2"+
		"\2\2\u00f9\u00fa\7\b\2\2\u00fa\17\3\2\2\2\u00fb\u00fc\5\22\n\2\u00fc\u0103"+
		"\b\t\1\2\u00fd\u00fe\7\35\2\2\u00fe\u00ff\5\22\n\2\u00ff\u0100\b\t\1\2"+
		"\u0100\u0102\3\2\2\2\u0101\u00fd\3\2\2\2\u0102\u0105\3\2\2\2\u0103\u0101"+
		"\3\2\2\2\u0103\u0104\3\2\2\2\u0104\21\3\2\2\2\u0105\u0103\3\2\2\2\u0106"+
		"\u0108\7\r\2\2\u0107\u0106\3\2\2\2\u0107\u0108\3\2\2\2\u0108\u0109\3\2"+
		"\2\2\u0109\u010b\7?\2\2\u010a\u010c\7\26\2\2\u010b\u010a\3\2\2\2\u010b"+
		"\u010c\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u010e\7?\2\2\u010e\u010f\b\n"+
		"\1\2\u010f\23\3\2\2\2\u0110\u0111\7\36\2\2\u0111\u0112\7\37\2\2\u0112"+
		"\u0117\7?\2\2\u0113\u0114\7\36\2\2\u0114\u0115\7 \2\2\u0115\u0117\7?\2"+
		"\2\u0116\u0110\3\2\2\2\u0116\u0113\3\2\2\2\u0117\25\3\2\2\2\u0118\u0119"+
		"\7\7\2\2\u0119\u011e\5\30\r\2\u011a\u011b\7\35\2\2\u011b\u011d\5\30\r"+
		"\2\u011c\u011a\3\2\2\2\u011d\u0120\3\2\2\2\u011e\u011c\3\2\2\2\u011e\u011f"+
		"\3\2\2\2\u011f\u0127\3\2\2\2\u0120\u011e\3\2\2\2\u0121\u0122\7\35\2\2"+
		"\u0122\u0123\7!\2\2\u0123\u0124\7\6\2\2\u0124\u0125\5*\26\2\u0125\u0126"+
		"\b\f\1\2\u0126\u0128\3\2\2\2\u0127\u0121\3\2\2\2\u0127\u0128\3\2\2\2\u0128"+
		"\u0129\3\2\2\2\u0129\u012a\7\b\2\2\u012a\27\3\2\2\2\u012b\u012c\5*\26"+
		"\2\u012c\u012d\7\6\2\2\u012d\u012e\5*\26\2\u012e\u012f\b\r\1\2\u012f\31"+
		"\3\2\2\2\u0130\u0131\7\"\2\2\u0131\u0132\5\36\20\2\u0132\u0133\b\16\1"+
		"\2\u0133\u0141\3\2\2\2\u0134\u0135\7#\2\2\u0135\u0136\5\32\16\2\u0136"+
		"\u0137\7$\2\2\u0137\u0138\b\16\1\2\u0138\u0141\3\2\2\2\u0139\u013a\5\36"+
		"\20\2\u013a\u013e\b\16\1\2\u013b\u013c\5\34\17\2\u013c\u013d\b\16\1\2"+
		"\u013d\u013f\3\2\2\2\u013e\u013b\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u0141"+
		"\3\2\2\2\u0140\u0130\3\2\2\2\u0140\u0134\3\2\2\2\u0140\u0139\3\2\2\2\u0141"+
		"\33\3\2\2\2\u0142\u0143\7%\2\2\u0143\u0144\5\32\16\2\u0144\u0145\b\17"+
		"\1\2\u0145\u014f\3\2\2\2\u0146\u0147\7&\2\2\u0147\u0148\5\32\16\2\u0148"+
		"\u0149\b\17\1\2\u0149\u014f\3\2\2\2\u014a\u014b\7\'\2\2\u014b\u014c\5"+
		"\32\16\2\u014c\u014d\b\17\1\2\u014d\u014f\3\2\2\2\u014e\u0142\3\2\2\2"+
		"\u014e\u0146\3\2\2\2\u014e\u014a\3\2\2\2\u014f\35\3\2\2\2\u0150\u0151"+
		"\7\26\2\2\u0151\u0152\7?\2\2\u0152\u017f\b\20\1\2\u0153\u0154\7\r\2\2"+
		"\u0154\u0157\7?\2\2\u0155\u0156\7\26\2\2\u0156\u0158\7?\2\2\u0157\u0155"+
		"\3\2\2\2\u0157\u0158\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u017f\b\20\1\2"+
		"\u015a\u015b\7(\2\2\u015b\u015c\7\r\2\2\u015c\u015f\7?\2\2\u015d\u015e"+
		"\7\26\2\2\u015e\u0160\7?\2\2\u015f\u015d\3\2\2\2\u015f\u0160\3\2\2\2\u0160"+
		"\u0161\3\2\2\2\u0161\u017f\b\20\1\2\u0162\u0164\7(\2\2\u0163\u0165\7\16"+
		"\2\2\u0164\u0163\3\2\2\2\u0164\u0165\3\2\2\2\u0165\u0166\3\2\2\2\u0166"+
		"\u0168\7?\2\2\u0167\u0169\5 \21\2\u0168\u0167\3\2\2\2\u0168\u0169\3\2"+
		"\2\2\u0169\u016a\3\2\2\2\u016a\u017f\b\20\1\2\u016b\u016c\7\16\2\2\u016c"+
		"\u016e\7?\2\2\u016d\u016f\5 \21\2\u016e\u016d\3\2\2\2\u016e\u016f\3\2"+
		"\2\2\u016f\u0170\3\2\2\2\u0170\u017f\b\20\1\2\u0171\u0172\7)\2\2\u0172"+
		"\u0173\7=\2\2\u0173\u017f\b\20\1\2\u0174\u0175\7*\2\2\u0175\u0178\7=\2"+
		"\2\u0176\u0177\7+\2\2\u0177\u0179\7=\2\2\u0178\u0176\3\2\2\2\u0178\u0179"+
		"\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017f\b\20\1\2\u017b\u017c\7,\2\2\u017c"+
		"\u017d\7?\2\2\u017d\u017f\b\20\1\2\u017e\u0150\3\2\2\2\u017e\u0153\3\2"+
		"\2\2\u017e\u015a\3\2\2\2\u017e\u0162\3\2\2\2\u017e\u016b\3\2\2\2\u017e"+
		"\u0171\3\2\2\2\u017e\u0174\3\2\2\2\u017e\u017b\3\2\2\2\u017f\37\3\2\2"+
		"\2\u0180\u0181\7-\2\2\u0181\u0182\5\"\22\2\u0182\u0183\b\21\1\2\u0183"+
		"\u019d\3\2\2\2\u0184\u0185\7.\2\2\u0185\u0186\5\"\22\2\u0186\u0187\b\21"+
		"\1\2\u0187\u019d\3\2\2\2\u0188\u0189\7/\2\2\u0189\u018a\5\"\22\2\u018a"+
		"\u018b\b\21\1\2\u018b\u019d\3\2\2\2\u018c\u018d\7\60\2\2\u018d\u018e\5"+
		"\"\22\2\u018e\u018f\b\21\1\2\u018f\u019d\3\2\2\2\u0190\u0191\7\61\2\2"+
		"\u0191\u0192\5\"\22\2\u0192\u0193\b\21\1\2\u0193\u019d\3\2\2\2\u0194\u0195"+
		"\7\62\2\2\u0195\u0196\5\"\22\2\u0196\u0197\b\21\1\2\u0197\u019d\3\2\2"+
		"\2\u0198\u0199\7\63\2\2\u0199\u019a\5\"\22\2\u019a\u019b\b\21\1\2\u019b"+
		"\u019d\3\2\2\2\u019c\u0180\3\2\2\2\u019c\u0184\3\2\2\2\u019c\u0188\3\2"+
		"\2\2\u019c\u018c\3\2\2\2\u019c\u0190\3\2\2\2\u019c\u0194\3\2\2\2\u019c"+
		"\u0198\3\2\2\2\u019d!\3\2\2\2\u019e\u019f\5&\24\2\u019f\u01a0\b\22\1\2"+
		"\u01a0\u01a8\3\2\2\2\u01a1\u01a2\7#\2\2\u01a2\u01a3\5$\23\2\u01a3\u01a4"+
		"\7$\2\2\u01a4\u01a5\5&\24\2\u01a5\u01a6\b\22\1\2\u01a6\u01a8\3\2\2\2\u01a7"+
		"\u019e\3\2\2\2\u01a7\u01a1\3\2\2\2\u01a8#\3\2\2\2\u01a9\u01ab\7\64\2\2"+
		"\u01aa\u01ac\7?\2\2\u01ab\u01aa\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac\u01b3"+
		"\3\2\2\2\u01ad\u01b3\7\65\2\2\u01ae\u01b3\7\66\2\2\u01af\u01b3\7\67\2"+
		"\2\u01b0\u01b3\78\2\2\u01b1\u01b3\79\2\2\u01b2\u01a9\3\2\2\2\u01b2\u01ad"+
		"\3\2\2\2\u01b2\u01ae\3\2\2\2\u01b2\u01af\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2"+
		"\u01b1\3\2\2\2\u01b3%\3\2\2\2\u01b4\u01b5\5*\26\2\u01b5\u01b6\b\24\1\2"+
		"\u01b6\u01c1\3\2\2\2\u01b7\u01b8\7\16\2\2\u01b8\u01b9\7?\2\2\u01b9\u01c1"+
		"\b\24\1\2\u01ba\u01bb\7@\2\2\u01bb\u01bc\7#\2\2\u01bc\u01bd\5(\25\2\u01bd"+
		"\u01be\7$\2\2\u01be\u01bf\b\24\1\2\u01bf\u01c1\3\2\2\2\u01c0\u01b4\3\2"+
		"\2\2\u01c0\u01b7\3\2\2\2\u01c0\u01ba\3\2\2\2\u01c1\'\3\2\2\2\u01c2\u01c3"+
		"\5\"\22\2\u01c3\u01ca\b\25\1\2\u01c4\u01c5\7\35\2\2\u01c5\u01c6\5\"\22"+
		"\2\u01c6\u01c7\b\25\1\2\u01c7\u01c9\3\2\2\2\u01c8\u01c4\3\2\2\2\u01c9"+
		"\u01cc\3\2\2\2\u01ca\u01c8\3\2\2\2\u01ca\u01cb\3\2\2\2\u01cb\u01ce\3\2"+
		"\2\2\u01cc\u01ca\3\2\2\2\u01cd\u01c2\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce"+
		")\3\2\2\2\u01cf\u01d0\7?\2\2\u01d0\u01da\b\26\1\2\u01d1\u01d2\7=\2\2\u01d2"+
		"\u01da\b\26\1\2\u01d3\u01d4\7<\2\2\u01d4\u01da\b\26\1\2\u01d5\u01d6\7"+
		">\2\2\u01d6\u01da\b\26\1\2\u01d7\u01d8\7:\2\2\u01d8\u01da\b\26\1\2\u01d9"+
		"\u01cf\3\2\2\2\u01d9\u01d1\3\2\2\2\u01d9\u01d3\3\2\2\2\u01d9\u01d5\3\2"+
		"\2\2\u01d9\u01d7\3\2\2\2\u01da+\3\2\2\2-\63;M\u0081\u0089\u0093\u0097"+
		"\u00a0\u00a6\u00af\u00ba\u00c3\u00c5\u00c9\u00d3\u00db\u00df\u00ee\u00f6"+
		"\u0103\u0107\u010b\u0116\u011e\u0127\u013e\u0140\u014e\u0157\u015f\u0164"+
		"\u0168\u016e\u0178\u017e\u019c\u01a7\u01ab\u01b2\u01c0\u01ca\u01cd\u01d9";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}