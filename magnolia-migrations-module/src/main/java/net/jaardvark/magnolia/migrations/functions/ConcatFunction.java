package net.jaardvark.magnolia.migrations.functions;

import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.jackrabbit.value.ValueFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcatFunction implements Function {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(ConcatFunction.class);
	
	
	
	@Override
	public Value apply(Value... params) {
		StringBuilder sb = new StringBuilder();
		for (Value v : params)
			try {
				if (v==null){
					log.warn("Cannot concatenate null value, adding nothing...");
					continue;
				}
				sb.append(v.getString());
			} catch (RepositoryException e) {
				log.warn("Problem getting value, not concating anything for this value.", e);
			}
		return  ValueFactoryImpl.getInstance().createValue(sb.toString());
	}

	@Override
	public Integer getExpectedNumberOfArguments() {
		return null; // any number of arguments
	}

	@Override
	public Value[] applyMultiple(Value... params) {
		throw new UnsupportedOperationException("ConcatFunction doesn't support multiple values.");
	}

	@Override
	public boolean isMultiple() {
		return false;
	}

}
