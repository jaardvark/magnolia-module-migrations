package net.jaardvark.magnolia.migrations.rules;


/**
 * Just holds a list of rules for later reuse via a "use" rule.
 * @author runger
 */
public class RulesRule implements RuleListRule {

	protected RuleList ruleList;

	protected String name;
	
	public RulesRule(String name, RuleList ruleList){
		this.name = name;
		this.ruleList = ruleList;
	}
		
	@Override
	public RuleList getRuleList() {
		return ruleList;
	}

	public String getName() {
		return name;
	}

}
