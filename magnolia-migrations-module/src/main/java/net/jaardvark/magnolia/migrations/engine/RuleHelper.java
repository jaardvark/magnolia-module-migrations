package net.jaardvark.magnolia.migrations.engine;

import org.apache.jackrabbit.commons.predicate.Predicate;

import net.jaardvark.jcr.predicate.PropertyPredicateBase;
import net.jaardvark.jcr.predicate.ValueProvider;
import net.jaardvark.magnolia.migrations.rules.ExcludePropertyRule;
import net.jaardvark.magnolia.migrations.rules.ExcludeRule;
import net.jaardvark.magnolia.migrations.rules.RenameNodeRule;
import net.jaardvark.magnolia.migrations.rules.RenamePropertyRule;
import net.jaardvark.magnolia.migrations.rules.Rule;
import net.jaardvark.magnolia.migrations.rules.SetChildPropertyRule;

public class RuleHelper {

	public static Rule renameNodeRule(String from, ValueProvider to) {
		if (from.contains("/"))
			throw new IllegalArgumentException("Rename rules cannot contain '/' characters in their arguments! Use a move rule to move nodes around.");
		return new RenameNodeRule(from, to);
	}

	public static Rule renamePropertyRule(String from, String to) {
		if (from.contains("/") || to.contains("/"))
			throw new IllegalArgumentException("Rename rules cannot contain '/' characters in their arguments! Use a move rule to move properties around.");
		return new RenamePropertyRule(from, to);
	}

	public static Rule setPropertyRule(String string, ValueProvider valueProvider) {
		if (string.contains("/"))
			return null; // TODO implement complex set-property
		return new SetChildPropertyRule(string, valueProvider);
	}

	public static Rule excludeRule(Predicate predicate) {
		if (predicate instanceof PropertyPredicateBase)
			return new ExcludePropertyRule(predicate);
		return new ExcludeRule(predicate);
	}

}
