package net.jaardvark.magnolia.migrations.rules;

import java.text.MessageFormat;

import javax.jcr.Value;

import org.apache.jackrabbit.commons.predicate.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jaardvark.jcr.predicate.ValueProvider;
import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

/**
 * Warns about matching nodes. Warning message is output if the rule gets to match the node,
 * so put warnings before other import rules in your script.
 * 
 * You can use the following "fields" in the output message:
 *  {0} - source node name
 *  {1} - source node path
 *  
 * 
 * @author Richard Unger
 */
public class WarnRule extends MatchingRuleBase implements ImportRule {
	
	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(WarnRule.class);

	protected ValueProvider message;

	public WarnRule(Predicate matchConditionPredicate, ValueProvider message) {
		super(matchConditionPredicate);
		this.message = message;
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {
		Value v = message.getValue(ctx.getSourceNode());
		String msgString = (v==null)?"null":v.toString();
		String output = MessageFormat.format(msgString, ctx.getSourceNode().getName(), ctx.getSourceNode().getPath());
		log.warn("MIGRATION WARNING - "+output);
		return ImportResult.NOT_MATCHED; // we always return not matched
	}

}
