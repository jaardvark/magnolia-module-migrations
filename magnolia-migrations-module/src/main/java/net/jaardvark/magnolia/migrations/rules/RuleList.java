package net.jaardvark.magnolia.migrations.rules;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Item;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

import com.google.common.collect.Iterables;

public class RuleList {
	
	protected List<Rule> rules = new ArrayList<Rule>();
	
	protected Map<String,RulesRule> rulesRules = new LinkedHashMap<String, RulesRule>();
	
	protected Map<String, MapRule> propertyMappingRules = new LinkedHashMap<String, MapRule>();
	
	protected Map<String, SetChildPropertyRule> propertySettingRules = new LinkedHashMap<String, SetChildPropertyRule>();	
	
	protected Map<String, RenamePropertyRule> propertyRenamingRules = new LinkedHashMap<String, RenamePropertyRule>();

	protected Map<String, RenameNodeRule> nodeRenamingRules = new LinkedHashMap<String, RenameNodeRule>();
	
	
	public ImportResult ruleListImportStage(MigrationContext ctx) throws Exception {
		
		// test import rules in turn
		for (ImportRule rule : getImportRules()){
			if (rule.matches(ctx.getSourceNode())){
				ImportResult result = rule.importStage(ctx);
				if (result!=ImportResult.NOT_MATCHED)
					return result; // if we matched, we're done here...
			}
		}
		
		// if we didn't find a matching rule
		return ImportResult.NOT_MATCHED;
	}


	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		for (PropertyDecidingRule rule : getPropertyDecidingRules()){
			if (rule.isPropertyExcluded(ctx, p)==ImportResult.EXCLUDED)
				return ImportResult.EXCLUDED;
		}
		// normally, properties get imported
		return ImportResult.NOT_MATCHED;
	}


	public String getPropertyName(String pName) {
		RenamePropertyRule rule = propertyRenamingRules.get(pName);
		if (rule!=null)
			return rule.getNewName();
		return pName;
	}

	
	// TODO mapPropertyValue does not consider use/if rules...
	public Value mapPropertyValue(String pName, Property p) throws RepositoryException {
		MapRule rule = propertyMappingRules.get(pName);
		if (rule!=null && rule.hasMappingFor(p))
			return rule.getPropertyValue(p);
		SetChildPropertyRule rule2 = propertySettingRules.get(pName);
		if (rule2!=null)
			return rule2.getPropertyValue(p);
		return p.getValue();
	}

	public Value mapValue(String pName, Value v) throws RepositoryException {
		MapRule rule = propertyMappingRules.get(pName);
		if (rule!=null && rule.hasMappingFor(v))
			return rule.mapValue(v);
		return v;
	}


	public String getNodeName(String name, Item evaluatedItem) {
		RenameNodeRule rule = nodeRenamingRules.get(name);
		if (rule!=null)
			return rule.getNewName(evaluatedItem);
		return name;
	}
	
	
	protected Iterable<ImportRule> getImportRules() {
		return Iterables.filter(getRules(), ImportRule.class);
	}

	protected Iterable<PropertyDecidingRule> getPropertyDecidingRules() {
		return Iterables.filter(getRules(), PropertyDecidingRule.class);		
	}
	
	

	public List<Rule> getRules() {
		return rules;
	}

	public void addRule(Rule rule){
		if (rule instanceof RulesRule){
			rulesRules.put(((RulesRule) rule).getName(), (RulesRule) rule);
			return;
		}
		
		if (rule instanceof MapRule)
			propertyMappingRules.put(((MapRule)rule).getPropertyName(), (MapRule)rule);
		else if (rule instanceof SetChildPropertyRule)
			propertySettingRules.put(((SetChildPropertyRule)rule).getPropertyName(), (SetChildPropertyRule)rule);
		else if (rule instanceof RenamePropertyRule)
			propertyRenamingRules.put(((RenamePropertyRule) rule).getPropertyName(), (RenamePropertyRule) rule);
		else if (rule instanceof RenameNodeRule)
			nodeRenamingRules.put(((RenameNodeRule) rule).getNodeName(), (RenameNodeRule)rule);

		rules.add(rule);
	}

	public RulesRule getUseRule(String reference) {
		return rulesRules.get(reference);
	}

}
