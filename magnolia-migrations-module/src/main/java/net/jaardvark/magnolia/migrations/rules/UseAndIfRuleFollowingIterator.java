package net.jaardvark.magnolia.migrations.rules;

import java.util.Iterator;
import java.util.Stack;

import net.jaardvark.magnolia.migrations.engine.MigrationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UseAndIfRuleFollowingIterator<T extends Rule> implements Iterable<T>, Iterator<T> {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(UseAndIfRuleFollowingIterator.class);
	
	public Class<T> returnedRuleType;
	
	protected StackElement current = null;
	
	protected Stack<StackElement> stack = new Stack<StackElement>();
	protected T next = null;
	
	public UseAndIfRuleFollowingIterator(Class<T> returnedRuleType, MigrationContext context){
		this.returnedRuleType = returnedRuleType;
		current = new StackElement(context);
		next = findNext();
	}
	
	
	@SuppressWarnings("unchecked")
	protected T findNext(){
		while (current!=null){
			// if we finished the current iterator, then pop...
			while (current!=null && !current.iterator.hasNext()){
				if (stack.isEmpty())
					return null;
				current = stack.pop();				
			}
			// continue in iterator
			while (current.iterator.hasNext()){
				Rule rule = current.iterator.next();
				if (rule instanceof UseRule){
					RulesRule rulesRule = current.context.resolveUse(((UseRule) rule).getReference());
					if (rulesRule==null)
						log.error("Cannot resolve use rule reference to: "+((UseRule) rule).getReference());
					else {
						stack.push(current);
						current = new StackElement(current.context.getChildContext(rulesRule.getRuleList()));
					}
				}
				else if (rule instanceof IfRule){
					IfRule ifRule = (IfRule) rule;
					if (ifRule.matches(current.context.getSourceNode())){
						stack.push(current);
						current = new StackElement(current.context.getChildContext(ifRule.getRuleList()));						
					}
				}
				else if (returnedRuleType.isAssignableFrom(rule.getClass()))
					return (T)rule;
			}
		}
		return null;
	}
	
	
	@Override
	public Iterator<T> iterator() {
		return this;
	}


	@Override
	public boolean hasNext() {
		return next!=null;
	}


	@Override
	public T next() {
		T currNext = next;
		next = findNext();
		return currNext;
	}


	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove elements from this iterator!");
	}
	
	
	protected static class StackElement {
		public StackElement(MigrationContext context) {
			this.context = context;
			this.iterator = context.getRuleList().getRules().iterator();
		}
		MigrationContext context;
		Iterator<Rule> iterator;
	}
	

}
