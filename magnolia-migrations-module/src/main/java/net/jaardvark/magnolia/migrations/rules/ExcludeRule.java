package net.jaardvark.magnolia.migrations.rules;

import org.apache.jackrabbit.commons.predicate.Predicate;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

/**
 * Exclude the matched nodes from being imported.
 * @author Richard Unger
 */
public class ExcludeRule extends MatchingRuleBase implements ImportRule {

	public ExcludeRule(Predicate matchConditionPredicate) {
		super(matchConditionPredicate);
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) {
		// match is assumed
		return ImportResult.EXCLUDED;
	}

}
