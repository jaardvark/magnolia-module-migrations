package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Item;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

public interface PropertyValueRule {

	Value getPropertyValue(Item p) throws RepositoryException;

	String getPropertyName();

}
