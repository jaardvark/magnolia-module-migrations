// Generated from JCRMigrationEngine.g4 by ANTLR 4.5.1

	package net.jaardvark.magnolia.migrations.parser;
	
	import net.jaardvark.magnolia.migrations.rules.*;
	import net.jaardvark.magnolia.migrations.rules.MapRule.ValueKey;
	import net.jaardvark.magnolia.migrations.engine.PredicateHelper;
	import net.jaardvark.magnolia.migrations.engine.RuleHelper;
	import net.jaardvark.magnolia.migrations.engine.ValueHelper;
	import net.jaardvark.magnolia.migrations.engine.MigrationScript;
	import org.apache.jackrabbit.commons.predicate.Predicate;
	import javax.jcr.Value;
	import java.util.Map;
	import java.util.LinkedHashMap;
	import net.jaardvark.jcr.predicate.NodeTypePredicate;
	import net.jaardvark.jcr.predicate.DepthPredicate;
	import net.jaardvark.jcr.predicate.NodePositionPredicate;
	import net.jaardvark.jcr.predicate.WorkspaceNamePredicate;
    import net.jaardvark.jcr.predicate.LiteralValueProvider;
    import net.jaardvark.jcr.predicate.CastingValueProvider;
	import net.jaardvark.jcr.predicate.PropertyValueProvider;
	import net.jaardvark.jcr.predicate.ValueProvider;
	import net.jaardvark.jcr.predicate.Operator;
	
	import static net.jaardvark.magnolia.migrations.engine.ValueHelper.str;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * This class provides an empty implementation of {@link JCRMigrationEngineListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class JCRMigrationEngineBaseListener implements JCRMigrationEngineListener {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMigrationScript(JCRMigrationEngineParser.MigrationScriptContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMigrationScript(JCRMigrationEngineParser.MigrationScriptContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMigrateStatement(JCRMigrationEngineParser.MigrateStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMigrateStatement(JCRMigrationEngineParser.MigrateStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMigrationRuleset(JCRMigrationEngineParser.MigrationRulesetContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMigrationRuleset(JCRMigrationEngineParser.MigrationRulesetContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMigrationStatement(JCRMigrationEngineParser.MigrationStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMigrationStatement(JCRMigrationEngineParser.MigrationStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterImportingRule(JCRMigrationEngineParser.ImportingRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitImportingRule(JCRMigrationEngineParser.ImportingRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCreateStatement(JCRMigrationEngineParser.CreateStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCreateStatement(JCRMigrationEngineParser.CreateStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCreateRuleset(JCRMigrationEngineParser.CreateRulesetContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCreateRuleset(JCRMigrationEngineParser.CreateRulesetContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterWrapStatements(JCRMigrationEngineParser.WrapStatementsContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitWrapStatements(JCRMigrationEngineParser.WrapStatementsContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterWrapStatement(JCRMigrationEngineParser.WrapStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitWrapStatement(JCRMigrationEngineParser.WrapStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterOrderSpec(JCRMigrationEngineParser.OrderSpecContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitOrderSpec(JCRMigrationEngineParser.OrderSpecContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMapRuleset(JCRMigrationEngineParser.MapRulesetContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMapRuleset(JCRMigrationEngineParser.MapRulesetContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMapStatement(JCRMigrationEngineParser.MapStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMapStatement(JCRMigrationEngineParser.MapStatementContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConditions(JCRMigrationEngineParser.ConditionsContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConditions(JCRMigrationEngineParser.ConditionsContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterConditionsTail(JCRMigrationEngineParser.ConditionsTailContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitConditionsTail(JCRMigrationEngineParser.ConditionsTailContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCondition(JCRMigrationEngineParser.ConditionContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCondition(JCRMigrationEngineParser.ConditionContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValueComparison(JCRMigrationEngineParser.ValueComparisonContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValueComparison(JCRMigrationEngineParser.ValueComparisonContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValue(JCRMigrationEngineParser.ValueContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValue(JCRMigrationEngineParser.ValueContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCastSpec(JCRMigrationEngineParser.CastSpecContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCastSpec(JCRMigrationEngineParser.CastSpecContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValueSpec(JCRMigrationEngineParser.ValueSpecContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValueSpec(JCRMigrationEngineParser.ValueSpecContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValueList(JCRMigrationEngineParser.ValueListContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValueList(JCRMigrationEngineParser.ValueListContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLiteralValue(JCRMigrationEngineParser.LiteralValueContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLiteralValue(JCRMigrationEngineParser.LiteralValueContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(TerminalNode node) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode(ErrorNode node) { }
}