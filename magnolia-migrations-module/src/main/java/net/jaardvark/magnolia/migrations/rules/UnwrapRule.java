package net.jaardvark.magnolia.migrations.rules;

import info.magnolia.jcr.iterator.NodeIterableAdapter;

import javax.jcr.Node;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

import org.apache.jackrabbit.commons.predicate.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Imports a node without including the node itself. So only imports its children...
 * @author Richard Unger
 */
public class UnwrapRule extends MatchingRuleBase implements ImportRuleListRule {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(UnwrapRule.class);
	
	protected RuleList ruleList;

	
	public UnwrapRule(Predicate matchConditionPredicate, RuleList ruleList) {
		super(matchConditionPredicate);
		this.ruleList = ruleList;
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {		
		Node notImportedNode = ctx.getSourceNode();
		log.debug("Unwrapping node "+notImportedNode.getName());
		
		RuleList importRuleList = (ruleList==null)?ctx.getRuleList():ruleList;
		
		// handle the node's children
		for (Node childNode : new NodeIterableAdapter(notImportedNode.getNodes())){
			log.debug("Considering child node '"+childNode.getName()+"'");
			MigrationContext childContext = ctx.getChildContext(childNode,importRuleList);
			importRuleList.ruleListImportStage(childContext);
		}

		// TODO create stage??
		
		return ImportResult.IMPORTED;
	}

	@Override
	public RuleList getRuleList() {
		return ruleList;
	}

}





