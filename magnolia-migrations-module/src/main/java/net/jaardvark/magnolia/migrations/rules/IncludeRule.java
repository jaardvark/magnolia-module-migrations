package net.jaardvark.magnolia.migrations.rules;


import info.magnolia.jcr.iterator.NodeIterableAdapter;
import info.magnolia.objectfactory.Components;

import java.util.HashSet;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.nodetype.NodeType;

import net.jaardvark.jcr.predicate.NamedPropertyPredicate;
import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;
import net.jaardvark.magnolia.migrations.engine.NodeCreator;

import org.apache.jackrabbit.JcrConstants;
import org.apache.jackrabbit.commons.predicate.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An include rule causes the matched nodes to be imported to the destination
 * node.
 * 
 * The rule can have a rule-list, and if it does, sub-nodes are processed using this
 * new rule list. Otherwise sub-nodes are processed using the currently active rule-list.
 * 
 * @author Richard Unger
 */
public class IncludeRule extends MatchingRuleBase implements ImportRuleListRule {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(IncludeRule.class);
	
	protected RuleList ruleList;

	protected NodeCreator nodeCreator;
	
	protected NamedPropertyPredicate excludedSystemProperties = new NamedPropertyPredicate(JcrConstants.JCR_UUID, JcrConstants.JCR_CREATED, JcrConstants.JCR_PRIMARYTYPE, "jcr:createdBy");
	
	
	public IncludeRule(Predicate matchConditionPredicate, RuleList ruleList) {
		super(matchConditionPredicate);
		this.ruleList = ruleList;
		this.nodeCreator = Components.getComponent(NodeCreator.class);
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {
		Node node = ctx.getSourceNode();
		String name = node.getName();
		// check for rename rules
		String newName = ctx.getRuleList().getNodeName(name, node);
		String primaryType = node.getPrimaryNodeType().getName();
		
		// create sub-context
		RuleList importRuleList = ctx.getRuleList();
		MigrationContext importContext = ctx;
		// if we have sub-rules, create a new context
		if (getRuleList()!=null){
			importRuleList = getRuleList();
			importContext = ctx.getChildContext(importRuleList);			
		}

		// map primary type
		Value newType = importRuleList.mapPropertyValue(JcrConstants.JCR_PRIMARYTYPE, node.getProperty(JcrConstants.JCR_PRIMARYTYPE));
		String newPrimaryType = newType.getString();
		String uuid = node.getIdentifier();
		if (ImportResult.EXCLUDED==importRuleList.isPropertyExcluded(importContext, node.getProperty(JcrConstants.JCR_UUID)))
			uuid = null;
		
		String checkedNewName = checkNodeName(ctx.getDestNode(), newName);
		if (checkedNewName.equals(newName))
			log.debug("Including node '"+name+"' ("+primaryType+") as '"+newName+"' ("+newPrimaryType+").");
		else
			log.warn("Including node '"+name+"' ("+primaryType+") as '"+checkedNewName+"' ("+newPrimaryType+"). Name '"+newName+"' already exists!");
		Node destNode = nodeCreator.createNode(ctx.getDestNode(), checkedNewName, newPrimaryType, uuid);
		
		// import properties TODO consider writing properties and just importing nodes... it would be faster
		Set<String> importedProperties = new HashSet<String>();
		PropertyIterator pi = node.getProperties();
		while (pi.hasNext()){
			Property p = pi.nextProperty();
			if (excludedSystemProperties.evaluate(p))
				continue;
			if (importRuleList.isPropertyExcluded(importContext, p)!=ImportResult.EXCLUDED){
				String pName = p.getName();
				// map rules and set rules for imported properties
				if (p.getName().equals(JcrConstants.JCR_MIXINTYPES)){
					for (NodeType mixin : node.getMixinNodeTypes())
						destNode.addMixin(mixin.getName());
				}
				else {
					// rename property
					String pNewName = importRuleList.getPropertyName(pName);
					if (p.isMultiple()){
						Value[] pValue = p.getValues();
						// TODO map multiple-value properties?
						log.debug("Including property "+pName+" as "+pNewName);
						destNode.setProperty(pNewName, pValue);
					}
					else {
						Value pValue = importRuleList.mapPropertyValue(pName, p);
						log.debug("Including property "+pName+" as "+pNewName);
						destNode.setProperty(pNewName, pValue);
					}					
					importedProperties.add(pNewName);
				}
			}
		}
		
		// import sub-nodes
		for (Node childNode : new NodeIterableAdapter(node.getNodes())){
			log.debug("Considering child node '"+childNode.getName()+"'");
			MigrationContext childContext = ctx.getChildContext(childNode,importRuleList,destNode);
			//ImportResult importResult = 
			// TODO what to do with the import result?
			importRuleList.ruleListImportStage(childContext);
		}

		// create stage
		createStage(importContext, importedProperties, destNode);
		
		log.info("Included node '"+name+"' as '"+checkedNewName+"'.");
		
		// set the destination in the context
		ctx.setDestNode(destNode);
		
		return ImportResult.IMPORTED;
	}
	
	
	
	
	protected String checkNodeName(Node destNode, String newName) throws RepositoryException {
		if (!destNode.hasNode(newName))
			return newName;
		int postfix = 0;
		String checkName = newName;
		do {
			checkName = newName + postfix++;
		} while (destNode.hasNode(checkName));
		return checkName;
	}
	
	
	
	

	protected void createStage(MigrationContext context, Set<String> importedProperties, Node node) throws Exception{
		// create rules
		UseAndIfRuleFollowingIterator<CreateRule> createRules = new UseAndIfRuleFollowingIterator<CreateRule>(CreateRule.class, context);
		for (CreateRule r : createRules){
			Node destNode = null;
			if (node.hasNode(r.getNodeName()))
				destNode = node.getNode(r.getNodeName());
			else
				destNode = nodeCreator.createNode(node, r.getNodeName(), r.getNodeType(), null);
			// recursive step
			if (r.getRuleList()!=null)
				createStage(context.getChildContext(r.getRuleList()), null, destNode);
		}
		
		// set property rules for properties that weren't already included
		UseAndIfRuleFollowingIterator<SetChildPropertyRule> setPropRules = new UseAndIfRuleFollowingIterator<SetChildPropertyRule>(SetChildPropertyRule.class, context);
		for (SetChildPropertyRule r : setPropRules){
			if (excludedSystemProperties.excludesName(r.getPropertyName()))
				continue;
			if (importedProperties==null || !importedProperties.contains(r.getPropertyName())){
				if (r.isMultiple(context.getSourceNode())){
					Value[] value = r.getPropertyValues(context.getSourceNode());
					// TODO multiple properties
					node.setProperty(r.getPropertyName(), value);
				}
				else {
					Value value = r.getPropertyValue(context.getSourceNode());
					value = context.getRuleList().mapValue(r.getPropertyName(), value);
					node.setProperty(r.getPropertyName(), value);					
				}
			}
		}
	}
	
	
	

	@Override
	public RuleList getRuleList() {
		return ruleList;
	}

}
