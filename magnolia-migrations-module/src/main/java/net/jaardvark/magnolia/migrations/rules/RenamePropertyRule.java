package net.jaardvark.magnolia.migrations.rules;

import info.magnolia.jcr.RuntimeRepositoryException;

import javax.jcr.Item;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public class RenamePropertyRule implements PropertyDecidingRule {

	protected String from;
	protected String to;

	public RenamePropertyRule(String from, String to){
		this.from = from;
		this.to = to;
		
	}
	
	@Override
	public boolean matches(Item item) {
		try {
			return item instanceof Property && item.getName().equals(from);
		} catch (RepositoryException e) {
			throw new RuntimeRepositoryException(e);
		}
	}

	@Override
	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		if (matches(p))
			return ImportResult.IMPORTED;
		return ImportResult.NOT_MATCHED;
	}

	public String getPropertyName(){
		return from;
	}
	
	public String getNewName(){
		return to;
	}
	
}
