package net.jaardvark.magnolia.migrations.rules;

public class CreateRule implements RuleListRule {

	protected RuleList ruleList;
	protected String nodeName;
	protected String nodeType;

	
	public CreateRule(String nodeName, String nodeType, RuleList ruleList){
		this.nodeName = nodeName;
		this.nodeType = nodeType;
		this.ruleList = ruleList;
	}
	
	
	@Override
	public RuleList getRuleList() {
		return ruleList;
	}
	
	public String getNodeName() {
		return nodeName;
	}

	public String getNodeType() {
		return nodeType;
	}

}
