package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Item;
import org.apache.jackrabbit.commons.predicate.Predicate;

public class MatchingRuleBase implements MatchingRule {

	protected Predicate matchConditionPredicate;
	
	
	public MatchingRuleBase(Predicate matchConditionPredicate){
		this.matchConditionPredicate = matchConditionPredicate;	
	}
	
	
	@Override
	public boolean matches(Item item) {
		return matchConditionPredicate.evaluate(item);
	}


	public Predicate getMatchConditionPredicate() {
		return matchConditionPredicate;
	}

}
