package net.jaardvark.magnolia.migrations.engine;

import java.io.IOException;

import javax.inject.Inject;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.magnolia.rendering.engine.RenderException;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.resourceloader.Resource;
import net.jaardvark.magnolia.migrations.parser.JCRMigrationEngineLexer;
import net.jaardvark.magnolia.migrations.parser.JCRMigrationEngineParser;
import net.jaardvark.magnolia.migrations.parser.JCRMigrationEngineParser.MigrationScriptContext;

/**
 * Runs migrations.
 * 
 * See documentation for migration-script class.
 * 
 * 
 * @author Richard Unger
 */
public class MigrationHelper {

	protected RenderingEngine renderingEngine;
	
	protected NodeCreator nodeCreator;

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MigrationHelper.class);

	@Inject
	public MigrationHelper(RenderingEngine engine, NodeCreator nodeCreator){
		this.renderingEngine = engine;
		this.nodeCreator = nodeCreator;
	}
	
	
	public MigrationScript loadMigrationScript(Resource resource) throws RenderException, MigrationScriptSyntaxException, IOException {
		
		// load named resource
		log.info("Loading Resource: "+resource.getPath());
		
		CharStream input = new ANTLRInputStream(resource.openReader());
		// parse
		JCRMigrationEngineLexer lexer = new JCRMigrationEngineLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		JCRMigrationEngineParser parser = new JCRMigrationEngineParser(tokens);
		MigrationScriptCompileErrorListener errors = new MigrationScriptCompileErrorListener();
		parser.addErrorListener(errors);
		
		MigrationScriptContext ctx = parser.migrationScript();
		if (errors.isError())
			throw new MigrationScriptSyntaxException(errors.getErrorMessages());
		
		// extract script object
		return ctx.script;
	}
	
	
	public void runMigrationScript(MigrationScript script) throws Exception{
		script.run();
	}


	public NodeCreator getNodeCreator() {
		return nodeCreator;
	}
	
	
}
