package net.jaardvark.magnolia.migrations.engine;

public enum ImportResult {
	IMPORTED,
	EXCLUDED,
	NOT_MATCHED
}
