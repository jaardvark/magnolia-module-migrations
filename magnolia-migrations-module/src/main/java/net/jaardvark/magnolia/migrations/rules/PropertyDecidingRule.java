package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Property;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public interface PropertyDecidingRule extends MatchingRule {

	ImportResult isPropertyExcluded(MigrationContext ctx, Property p);

}
