package net.jaardvark.magnolia.migrations.functions;

import javax.jcr.Value;

public interface Function {

	public Value apply(Value...params);
	
	public Value[] applyMultiple(Value...params);
	
	/**
	 * Return a positive integer for exact number of arguments, 0 for no arguments,
	 * null for any number of arguments (including none) and negative integers meaning up to that number
	 * of arguments or less.
	 * @return an integer, or null.
	 */
	public Integer getExpectedNumberOfArguments();

	public boolean isMultiple();
	
}
