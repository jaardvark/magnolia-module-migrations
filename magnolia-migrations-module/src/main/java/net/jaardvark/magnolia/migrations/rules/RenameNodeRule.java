package net.jaardvark.magnolia.migrations.rules;

import info.magnolia.jcr.RuntimeRepositoryException;

import javax.jcr.Item;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jaardvark.jcr.predicate.ValueProvider;



public class RenameNodeRule implements Rule {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(RenameNodeRule.class);
	
	protected String from;
	protected ValueProvider to;

	public RenameNodeRule(String from, ValueProvider to){
		this.from = from;
		this.to = to;
		
	}

	public String getNodeName(){
		return from;
	}

	public String getNewName(Item evaluatedItem){
		try {
			Value val = to.getValue(evaluatedItem);
			if (val==null){
				log.warn("Could not get value for node rename.");
				return from;
			}
			return val.getString();
		} catch (RepositoryException e) {
			throw new RuntimeRepositoryException(e);
		}
	}
}
