package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Item;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public class NonConsumingRule implements ImportRuleListRule {

	protected ImportRuleListRule rule;

	public NonConsumingRule(ImportRuleListRule rule){
		this.rule = rule;
	}
	
	@Override
	public boolean matches(Item item) {
		return rule.matches(item);
	}

	@Override
	public RuleList getRuleList() {
		return rule.getRuleList();
	}

	@Override
	public ImportResult importStage(MigrationContext ctx) throws Exception {
		rule.importStage(ctx);
		// as a non-consuming rule, we return not matched, no matter what...
		return ImportResult.NOT_MATCHED;
	}

}
