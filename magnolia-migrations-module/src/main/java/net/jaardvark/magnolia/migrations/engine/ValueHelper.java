package net.jaardvark.magnolia.migrations.engine;

import java.util.List;

import info.magnolia.objectfactory.Components;

import javax.jcr.Value;

import net.jaardvark.jcr.predicate.ValueProvider;
import net.jaardvark.magnolia.migrations.functions.Function;
import net.jaardvark.magnolia.migrations.functions.FunctionValueProvider;
import net.jaardvark.magnolia.migrations.module.MigrationsModule;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.jackrabbit.value.ValueFactoryImpl;

public class ValueHelper {
	
	public static Value doubleValue(String val){
		return ValueFactoryImpl.getInstance().createValue(Double.valueOf(val));
	}

	public static Value booleanValue(String string) {
		return ValueFactoryImpl.getInstance().createValue(Boolean.valueOf(string));
	}

	public static Value longValue(String string) {
		return ValueFactoryImpl.getInstance().createValue(Long.valueOf(string));
	}

	public static Value stringValue(String string) {
		string = StringEscapeUtils.unescapeJava(string);
		return ValueFactoryImpl.getInstance().createValue(string);
	}

	public static String str(String string) {
		if (string==null)
			return null;
		if (string.length()>2)
			return string.substring(1,string.length()-1);
		return "";
	}
	
	public static ValueProvider function(String functionName, List<ValueProvider> inputValues) {
		MigrationsModule module = Components.getComponent(MigrationsModule.class);
		Function f = module.getFunctions().get(functionName);
		if (f==null)
			throw new IllegalArgumentException("The function name '"+functionName+"' was not found.");
		return new FunctionValueProvider(f, inputValues.toArray(new ValueProvider[inputValues.size()]));
	}
	
}
