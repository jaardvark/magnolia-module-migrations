package net.jaardvark.magnolia.migrations.functions;

import info.magnolia.jcr.RuntimeRepositoryException;

import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.jackrabbit.value.ValueFactoryImpl;

/**
 * Removes any occurrences of the arguments 1 to N from the argument 0.
 * @author Richard Unger
 */
public class RemoveStringsFunction implements Function {


	@Override
	public Integer getExpectedNumberOfArguments() {
		return null; // any number of arguments
	}

	@Override
	public Value apply(Value... params) {
		if (params==null || params.length < 1)
			return null;
		try {
			if (params[0]==null)
				return null;
			String valToReplace = params[0].getString();
			if (valToReplace==null)
				return null;
			int pos = 1;
			while (pos<params.length)
				valToReplace = valToReplace.replaceAll(params[pos++].getString(), "");		
			return ValueFactoryImpl.getInstance().createValue(valToReplace);
		} catch (RepositoryException e) {
			throw new RuntimeRepositoryException(e);
		}
	}

	@Override
	public Value[] applyMultiple(Value... params) {
		throw new UnsupportedOperationException("RemoveStringsFunction doesn't support multiple values.");
	}

	@Override
	public boolean isMultiple() {
		return false;
	}

}
