package net.jaardvark.magnolia.migrations.engine;

import info.magnolia.jcr.RuntimeRepositoryException;
import info.magnolia.jcr.iterator.NodeIterableAdapter;
import info.magnolia.jcr.util.SessionUtil;
import info.magnolia.jcr.wrapper.DelegateNodeWrapper;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import net.jaardvark.magnolia.migrations.rules.ImportRule;
import net.jaardvark.magnolia.migrations.rules.Rule;
import net.jaardvark.magnolia.migrations.rules.RuleList;
import net.jaardvark.magnolia.migrations.rules.RuleListRule;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A migration script.
 * 
 * The logic for executing a migration script is as follows:
 *  
 *  The "migration" statement defines a name for the migration script.
 *  
 *  Note: a "rules" block just defines some rules for later use with "include" elsewhere
 *  An "include" rule includes the rules of the named "rules" block as if they were written at
 *  that point.
 *  
 *  The "migrate" statements determine what gets migrated from where to where.
 *  They are executed in the order found in the script.
 *  
 *  Each "migrate" statement causes the following to happen:
 *  
 *  1. Import stage
 *  
 *  - from the source workspace given, beginning at the node indicated (or the jcr:root, if no path is given)
 *  - for each of the node's child items, ("main loop") the script's rules are applied in order:
 *    
 *    - "workspace" rules match if the item comes from the specified workspace
 *      	effect is to apply the "workspace" rule's ruleset to the current item
 *    - "for" rules match if their condition fits the current item, which must be a node
 *    		effect is that the current node is imported, and the for rule's ruleset is
 *    		applied to the node's children during import, a "for loop" - see details below
 *    - "include" rules match if their condition fits 
 *    		causes the current item and its subtree to be imported without further processing (copy)
 *    - "exclude" rules match if their condition fits
 *    		cause the current item not to be imported
 *    - "map" rules are applied during import
 *    		matching properties values are mapped according to the rule
 *    - "set" rules with literal values set their values during import
 *    - "create" rules create new nodes during import
 *    		nodes are created after node's children have been processed by "for", "exclude" and "include"
 *    - "move" rules that are simply renames of nodes or properties are applied during import
 *    
 *  - "set" rules with variable values (referencing other properties), "move" rules with complex semantics
 *    and "delete" rules are ignored in this stage, see "Transformation stage", below.
 *  
 *  - an item is handled by the first matching "include", "exclude" or "for" rule
 *  
 *  - when processing the items, after all "include", "exclude" or "for" rules have been considered
 *    for the item, nodes not matching any rule are ignored, while properties not matching
 *    any rule are imported
 *    
 *  - after the "main loop" processes an item that is a node, the JCR session is saved, comitting the
 *    changes.
 *  - after processing an item, if the item was a node, and was imported, processing continues with
 *    that node's children (recursive step). Otherwise processing continues with the next item.
 *  
 *  - note that a node should be imported either by the recursive processing of the "main loop", OR
 *    by the ruleset of a "for" rule, but (presumably) not both. Write your rules accordingly.
 *  
 *  2. Transformation stage
 * 
 * 	This stage is performed for each migration after the import stage is completed for the migration.
 *  In the transformation stage, the complex "set" and "move" rules are performed, as well as
 *  the "delete" rules, in the order they are given in the script.
 *  
 *  This allows more complex transformations of the content not possible during the import stage.
 * 
 * Understanding the "main" and "for" loops
 * 
 * Consider that there are several "scopes" working together during a migration:
 *  - the "main loop" scope - each node from the given root is evaluated in turn against the
 *    outermost ruleset (the migration script itself).
 *  - the "for loop" scope - the node from the main loop is being imported. It's child nodes may
 *    be part of the import due to "include" or "for" rules.
 *  - the "ruleset" scope - due to "for" and "create" rules, the current ruleset can change as the
 *    import processes child nodes. Note that "for" rules can be nested!
 * 
 * 
 * 
 * @author Richard Unger
 */
public class MigrationScript implements ImportRule, RuleListRule {
	
	protected RuleList ruleList = new RuleList();
	
	protected String name;
	
	protected Map<String,String> migrate = new LinkedHashMap<String,String>();
	
	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MigrationScript.class);
	
	
	
	
	public MigrationScript(String name) {
		this.name = name;
	}

	


	/**
	 * Runs the migration script
	 * @throws RepositoryException 
	 */
	protected void run() throws Exception{
		for (Entry<String, String> entry : migrate.entrySet()){
			String sourceWorkspace = null;
			String destWorkspace = null;
			String sourcePath = null;
			String destPath = null;
			// determine source
			if (entry.getKey().contains("/")){
				sourceWorkspace = StringUtils.substringBefore(entry.getKey(), "/");
				sourcePath = StringUtils.substringAfter(entry.getKey(), "/");
				if (!sourcePath.startsWith("/"))
					sourcePath = "/" + sourcePath;
			}
			else {
				sourcePath = "/";
				sourceWorkspace = entry.getKey();
			}
			// determine destination
			if (entry.getValue().contains("/")){
				destWorkspace = StringUtils.substringBefore(entry.getValue(), "/");
				destPath = StringUtils.substringAfter(entry.getValue(), "/");
				if (!destPath.startsWith("/"))
					destPath = "/" + destPath;
			}
			else {
				destPath = "/";
				destWorkspace = entry.getValue();
			}
			Node sourceNode = SessionUtil.getNode(sourceWorkspace, sourcePath);
			Node destNode = SessionUtil.getNode(destWorkspace, destPath);
			if (sourceNode==null)
				throw new PathNotFoundException("The source "+sourcePath+" was not found in workspace "+sourceWorkspace);
			if (destNode==null)
				throw new PathNotFoundException("The destination "+destPath+" was not found in workspace "+destWorkspace);
			// unwrap them - migrations work on raw JCR nodes
			while (sourceNode instanceof DelegateNodeWrapper)
				sourceNode = ((DelegateNodeWrapper)sourceNode).getWrappedNode();
			while (destNode instanceof DelegateNodeWrapper)
				destNode = ((DelegateNodeWrapper)destNode).getWrappedNode();
			
			// create initial context
			MigrationContext ctx = new MigrationContext(null, sourceNode, destNode, getRuleList());			
			// run the import stage
			importStage(ctx);
			// after import, run the create stage for the main script
			createStage(ctx);			
			// finally, run the transformation stage
			transformationStage(ctx);
		}
	}

	
	
	@Override
	public ImportResult importStage(MigrationContext context) throws Exception {
		
		// iterate over children of source
		for (Node childNode : new NodeIterableAdapter(context.getSourceNode().getNodes())){
			
			log.debug("Considering node for import: '"+childNode.getName()+"'.");
			
			// for each child,
			MigrationContext childContext = context.getChildContext(childNode);
			ImportResult importResult = getRuleList().ruleListImportStage(childContext);
			if (importResult==ImportResult.IMPORTED){
				// save
				try {
					context.getDestNode().getSession().save();
					log.info("Imported and saved node '"+childNode.getName()+"'.");
				} catch (RepositoryException e) {
					log.error("Could not save node: "+context.getDestNode(),e);
				}
				// recursive step
				try {				
					importStage(childContext);
				} catch (Exception e) {
					log.error("Error importing children of "+context.getDestNode()+": "+e.getMessage(),e);
				}
			}
		}

		return ImportResult.NOT_MATCHED; // return not matched
	}

	
	

	protected void createStage(MigrationContext context) {
		// TODO Auto-generated method stub
		
	}
	
	
	protected void transformationStage(MigrationContext context) {
		// TODO Auto-generated method stub
		
	}

	
	
	
	
	/**
	 * This isn't used on this rule
	 */
	@Override
	public boolean matches(Item item) {
		return false;
	}
	

	@Override
	public RuleList getRuleList() {
		return ruleList;
	}


	public void addRule(Rule rule){
		ruleList.addRule(rule);
	}


	public void addMigrate(String from, String to) {
		migrate.put(from, to);
	}
	
	
}
