package net.jaardvark.magnolia.migrations.engine;

public class MigrationScriptSyntaxException extends Exception {
	private static final long serialVersionUID = 1L;

	public MigrationScriptSyntaxException() {
		super();
	}

	public MigrationScriptSyntaxException(String message, Throwable cause) {
		super(message, cause);
	}

	public MigrationScriptSyntaxException(String message) {
		super(message);
	}

	public MigrationScriptSyntaxException(Throwable cause) {
		super(cause);
	}

}
