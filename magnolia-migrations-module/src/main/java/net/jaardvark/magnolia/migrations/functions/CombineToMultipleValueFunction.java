package net.jaardvark.magnolia.migrations.functions;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Value;

public class CombineToMultipleValueFunction implements Function {

	@Override
	public Value apply(Value... params) {
		throw new UnsupportedOperationException("CombineToMultipleValueFunction doesn't support single values.");
	}

	@Override
	public Value[] applyMultiple(Value... params) {
		List<Value> result = new ArrayList<Value>();
		for (Value v : params)
			if (v!=null)
				result.add(v);
		return result.toArray(new Value[result.size()]);
	}

	@Override
	public Integer getExpectedNumberOfArguments() {
		return null;
	}

	@Override
	public boolean isMultiple() {
		return true;
	}

}
