package net.jaardvark.magnolia.migrations.rules;


public interface RuleListRule extends Rule {

	RuleList getRuleList();
	
}
