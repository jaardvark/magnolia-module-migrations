package net.jaardvark.magnolia.migrations.rules;

import javax.jcr.Property;

import org.apache.jackrabbit.commons.predicate.Predicate;

import net.jaardvark.magnolia.migrations.engine.ImportResult;
import net.jaardvark.magnolia.migrations.engine.MigrationContext;

public class ExcludePropertyRule extends MatchingRuleBase implements PropertyDecidingRule {

	public ExcludePropertyRule(Predicate matchConditionPredicate) {
		super(matchConditionPredicate);
	}

	@Override
	public ImportResult isPropertyExcluded(MigrationContext ctx, Property p) {
		if (matches(p))
			return ImportResult.EXCLUDED;
		return ImportResult.NOT_MATCHED;
	}

}
