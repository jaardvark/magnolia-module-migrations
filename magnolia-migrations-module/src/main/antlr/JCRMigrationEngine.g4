
grammar JCRMigrationEngine;

@header {
	package net.jaardvark.magnolia.migrations.parser;
	
	import net.jaardvark.magnolia.migrations.rules.*;
	import net.jaardvark.magnolia.migrations.rules.MapRule.ValueKey;
	import net.jaardvark.magnolia.migrations.engine.PredicateHelper;
	import net.jaardvark.magnolia.migrations.engine.RuleHelper;
	import net.jaardvark.magnolia.migrations.engine.ValueHelper;
	import net.jaardvark.magnolia.migrations.engine.MigrationScript;
	import org.apache.jackrabbit.commons.predicate.Predicate;
	import javax.jcr.Value;
	import java.util.Map;
	import java.util.LinkedHashMap;
	import net.jaardvark.jcr.predicate.NodeTypePredicate;
	import net.jaardvark.jcr.predicate.DepthPredicate;
	import net.jaardvark.jcr.predicate.NodePositionPredicate;
	import net.jaardvark.jcr.predicate.WorkspaceNamePredicate;
    import net.jaardvark.jcr.predicate.LiteralValueProvider;
    import net.jaardvark.jcr.predicate.CastingValueProvider;
	import net.jaardvark.jcr.predicate.PropertyValueProvider;
	import net.jaardvark.jcr.predicate.ValueProvider;
	import net.jaardvark.jcr.predicate.Operator;
	
	import static net.jaardvark.magnolia.migrations.engine.ValueHelper.str;
}

migrationScript returns [MigrationScript script] 
					: 'migration' STRING { $script = new MigrationScript(str($STRING.text)); } ';' 
					  ( migrateStatement[$script] )* 
					  ( migrationStatement { $script.addRule($migrationStatement.rule); } )* 
					  EOF;


migrateStatement [MigrationScript script]
					 : 	'migrate' f=STRING '->' t=STRING ';' { $script.addMigrate(str($f.text), str($t.text)); } ;


migrationRuleset returns [RuleList ruleList] @init{ $ruleList = new RuleList(); }
				: '{' ( it=migrationStatement { $ruleList.addRule($it.rule); } )* '}' ;


migrationStatement returns [Rule rule]
					:	'exclude' conditions ';'								{ $rule = RuleHelper.excludeRule($conditions.predicate); }
					|	'delete' conditions ';'									
					|	'rules' STRING migrationRuleset 						{ $rule = new RulesRule(str($STRING.text), $migrationRuleset.ruleList); }
					|	'rename' 'node' f=STRING '->' v=value ';'				{ $rule = RuleHelper.renameNodeRule(str($f.text),$v.valueProvider); }
					|	'rename' 'property' f=STRING '->' t=STRING ';'			{ $rule = RuleHelper.renamePropertyRule(str($f.text),str($t.text)); }
					|	'use' STRING ';' 										{ $rule = new UseRule(str($STRING.text)); }
					|	importingRule											{ $rule = $importingRule.rule; }
					|	'non-consuming' importingRule							{ $rule = new NonConsumingRule($importingRule.rule); }
					|	'if' conditions migrationRuleset						{ $rule = new IfRule($conditions.predicate, $migrationRuleset.ruleList); }
					|	'map' 'property'? STRING mapRuleset						{ $rule = new MapRule(str($STRING.text), $mapRuleset.mapping, $mapRuleset.hadDefault, $mapRuleset.defaultMapping); }
					| 	'set' 'property'? STRING '=' value ';'					{ $rule = RuleHelper.setPropertyRule(str($STRING.text), $value.valueProvider); }
					|	'create' 'node'? n=STRING 'type'? t=STRING createRuleset	{ $rule = new CreateRule(str($n.text), str($t.text), $createRuleset.ruleList); }
					|	'warn' conditions 'message'? value ';'					{ $rule = new WarnRule($conditions.predicate, $value.valueProvider); }
					;
					
importingRule returns [ImportRuleListRule rule]
					:	'include' conditions ( migrationRuleset 				{ $rule = new IncludeRule($conditions.predicate, $migrationRuleset.ruleList); } 
											 | ';' 								{ $rule = new IncludeRule($conditions.predicate, null); } 
											 )
					|	'wrap' conditions 'in' wrapStatements ( migrationRuleset { $rule = new WrapRule($conditions.predicate, $migrationRuleset.ruleList, $wrapStatements.weList); }
											 				  | ';' 			{ $rule = new WrapRule($conditions.predicate, null, $wrapStatements.weList); } 
											 				  )
					|	'unwrap' conditions ( migrationRuleset					{ $rule = new UnwrapRule($conditions.predicate, $migrationRuleset.ruleList); }
															  | ';'				{ $rule = new UnwrapRule($conditions.predicate, null); }
															  )
					;


					
createStatement returns [Rule rule]
					:	'set' 'property'? STRING '=' value ';'						{ $rule = RuleHelper.setPropertyRule(str($STRING.text), $value.valueProvider); }
					|	'map' 'property'? STRING mapRuleset							{ $rule = new MapRule(str($STRING.text), $mapRuleset.mapping, $mapRuleset.hadDefault, $mapRuleset.defaultMapping); }
					|	'create' 'node'? n=STRING 'type'? t=STRING createRuleset	{ $rule = new CreateRule(str($n.text), str($t.text), $createRuleset.ruleList); }
					|	'if' conditions createRuleset								{ $rule = new IfRule($conditions.predicate, $createRuleset.ruleList); }
					|	'use' STRING ';' 											{ $rule = new UseRule(str($STRING.text)); }
					;
					
					
createRuleset returns [RuleList ruleList] @init{ $ruleList = new RuleList(); }
				: 	'{' ( it=createStatement { $ruleList.addRule($it.rule); } )* '}' ;

wrapStatements returns [List<WrapRule.WrappingNode> weList] @init{ $weList=new ArrayList<WrapRule.WrappingNode>(); }
				:	wrapStatement { $weList.add($wrapStatement.we); } ( ',' wrapStatement { $weList.add($wrapStatement.we); } )*
				;
				
wrapStatement returns [WrapRule.WrappingNode we]
				:	'node'? n=STRING 'type'? t=STRING { $we = new WrapRule.WrappingNode(str($n.text), str($t.text)); }
				;

orderSpec : 	'order' 'after' STRING
			|	'order' 'before' STRING
			;


mapRuleset returns [Map<ValueKey,Value> mapping, Value defaultMapping, boolean hadDefault] @init{ $mapping = new LinkedHashMap<ValueKey,Value>(); $hadDefault=false; $defaultMapping=null; }
			: '{' mapStatement[$mapping] ( ',' mapStatement[$mapping] )* ( ',' 'default' '->' literalValue { $hadDefault=true; $defaultMapping=$literalValue.jcrValue; } )? '}' ;


mapStatement [Map<ValueKey,Value> mapping] 
			: f=literalValue '->' t=literalValue { $mapping.put(new ValueKey($f.jcrValue),$t.jcrValue); } ;


conditions returns [Predicate predicate]
			:	'not' condition 													{ $predicate = PredicateHelper.negate($condition.predicate); }
			|	'(' c=conditions ')' 													{ $predicate = $c.predicate; }
			|	condition { $predicate = $condition.predicate; } ( conditionsTail[$condition.predicate] { $predicate = $conditionsTail.predicate; } )?
			;
			
conditionsTail [Predicate leftSide] returns [Predicate predicate]
				:	'and' conditions 		{ $predicate = PredicateHelper.conjunction($leftSide, $conditions.predicate); }
				| 	'or' conditions 		{ $predicate = PredicateHelper.disjunction($leftSide, $conditions.predicate); }
				| 	'xor' conditions		{ $predicate = PredicateHelper.xor($leftSide, $conditions.predicate); }
				;

condition returns [Predicate predicate]
			:	'type' STRING									{ $predicate = new NodeTypePredicate(str($STRING.text)); }
			|	'node' n=STRING ('type' t=STRING)?				{ $predicate = PredicateHelper.nodeNamePredicate(str($n.text),str($t.text)); }
			|	'with' 'node' n=STRING ('type' t=STRING)?		{ $predicate = PredicateHelper.withNodePredicate(str($n.text),str($t.text)); }
			|	'with' 'property'? STRING v=valueComparison?	{ $predicate = PredicateHelper.withPropertyPredicate(str($STRING.text), ($v.ctx!=null)?$v.operator:null, ($v.ctx!=null)?$v.valueProvider:null); }
			|	'property' STRING v=valueComparison?			{ $predicate = PredicateHelper.propertyPredicate(str($STRING.text), ($v.ctx!=null)?$v.operator:null, ($v.ctx!=null)?$v.valueProvider:null); }
			|	'position' LONG									{ $predicate = new NodePositionPredicate(Long.valueOf($LONG.text)); }
			|	'depth' d1=LONG ( 'to' d2=LONG )?				{ $predicate = PredicateHelper.depthPredicate(Long.valueOf($d1.text), ($d2.text!=null)?Long.valueOf($d2.text):null); }
			|	'workspace' STRING								{ $predicate = new WorkspaceNamePredicate(str($STRING.text)); }
			;

valueComparison returns [Operator operator, ValueProvider valueProvider]
				:	'==' value 		{ $operator=Operator.EQUAL; $valueProvider=$value.valueProvider; }
				|	'>' value 		{ $operator=Operator.GT; $valueProvider=$value.valueProvider; }
				|	'<' value 		{ $operator=Operator.LT; $valueProvider=$value.valueProvider; }
				|	'>=' value 		{ $operator=Operator.GT_EQ; $valueProvider=$value.valueProvider; }
				|	'<=' value 		{ $operator=Operator.LT_EQ; $valueProvider=$value.valueProvider; }
				|	'!=' value 		{ $operator=Operator.NOTEQUAL; $valueProvider=$value.valueProvider; }
				|	'~=' value 		{ $operator=Operator.REGEX; $valueProvider=$value.valueProvider; }
				;

value returns [ValueProvider valueProvider]
			:	valueSpec					{ $valueProvider = $valueSpec.valueProvider; }
			|	'(' castSpec ')' valueSpec  { $valueProvider = new CastingValueProvider($castSpec.text, $valueSpec.valueProvider); }
			;

castSpec 	:	'Date' STRING?
			|	'Boolean'
			|	'Long'
			|	'String'
			|	'Double'
			|	'Binary'
			;

valueSpec returns [ValueProvider valueProvider]
			:	literalValue 				{ $valueProvider = new LiteralValueProvider($literalValue.jcrValue); }
			|	'property' STRING			{ $valueProvider = new PropertyValueProvider(str($STRING.text)); }
			|	FUNCNAME '(' valueList ')'	{ $valueProvider = ValueHelper.function($FUNCNAME.text, $valueList.valueProviders ); }
			;

valueList returns [List<ValueProvider> valueProviders] @init{ $valueProviders = new ArrayList<ValueProvider>(); }
			:	( v1=value { $valueProviders.add($v1.valueProvider); } ( ',' v2=value { $valueProviders.add($v2.valueProvider); } )* )?
			;

literalValue returns [Value jcrValue] 
			: 	STRING 	{ $jcrValue = ValueHelper.stringValue(str($STRING.text)); }
			|	LONG 	{ $jcrValue = ValueHelper.longValue($LONG.text); }
			|	BOOL	{ $jcrValue = ValueHelper.booleanValue($BOOL.text); }
			|	DOUBLE	{ $jcrValue = ValueHelper.doubleValue($DOUBLE.text); }
			|	'null'	{ $jcrValue = null; }
			;
					
WS : [ \r\t\n]+ -> skip ;

BOOL : 'true' | 'false' ;

LONG : '-'?[0-9]+ ;

DOUBLE : '-'?[0-9]+'.'[0-9]+ ;

STRING : '"' ~["]* '"' | '\'' ~[\']* '\'';

FUNCNAME : [a-zA-Z][a-zA-Z0-9]* ;

BlockComment :   '/*' .*? '*/'  -> skip ;

LineComment :   '//' ~[\r\n]* -> skip ;

